<?php
/**
 *
 * The framework's functions and definitions
 */

/**
 * ------------------------------------------------------------------------------------------------
 * Define constants.
 * ------------------------------------------------------------------------------------------------
 */
update_option( 'woodmart_is_activated', '1' );
use Elementor\Utils;

define( 'WOODMART_THEME_DIR', get_template_directory_uri() );
define( 'WOODMART_THEMEROOT', get_template_directory() );
define( 'WOODMART_IMAGES', WOODMART_THEME_DIR . '/images' );
define( 'WOODMART_SCRIPTS', WOODMART_THEME_DIR . '/js' );
define( 'WOODMART_STYLES', WOODMART_THEME_DIR . '/css' );
define( 'WOODMART_FRAMEWORK', '/inc' );
define( 'WOODMART_DUMMY', WOODMART_THEME_DIR . '/inc/dummy-content' );
define( 'WOODMART_CLASSES', WOODMART_THEMEROOT . '/inc/classes' );
define( 'WOODMART_CONFIGS', WOODMART_THEMEROOT . '/inc/configs' );
define( 'WOODMART_HEADER_BUILDER', WOODMART_THEME_DIR . '/inc/header-builder' );
define( 'WOODMART_ASSETS', WOODMART_THEME_DIR . '/inc/admin/assets' );
define( 'WOODMART_ASSETS_IMAGES', WOODMART_ASSETS . '/images' );
define( 'WOODMART_API_URL', 'https://xtemos.com/licenses/api/' );
define( 'WOODMART_DEMO_URL', 'https://woodmart.xtemos.com/' );
define( 'WOODMART_PLUGINS_URL', WOODMART_DEMO_URL . 'plugins/' );
define( 'WOODMART_DUMMY_URL', WOODMART_DEMO_URL . 'dummy-content-new/' );
define( 'WOODMART_SLUG', 'woodmart' );
define( 'WOODMART_CORE_VERSION', '1.0.31' );
define( 'WOODMART_WPB_CSS_VERSION', '1.0.2' );

// удаляем описание категории на странице категорий
remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
// выводим описание категории под товарами
add_action( 'woocommerce_after_shop_loop', 'woocommerce_taxonomy_archive_description', 100 );



// Добавление в хлебные крошки ссылки на страницу магазина WooCommerce
add_filter( 'woocommerce_get_breadcrumb', function($crumbs, $breadcrumb){
	$shop_page_id = wc_get_page_id('shop'); // Запрашиваем ID страницы магазина
	if($shop_page_id > 0 && !is_shop() && (is_product_taxonomy() || is_product())) { // Проверяем корректность полученного ID и чтобы текущая страница не была страницей магазина и то, что страница является страницей товара или категорией товаров
		$new_breadcrumb = [
			//_x( 'Магазин', 'breadcrumb', 'woocommerce' ), // Строка для добавления собственного названия "Магазина". Для использования раскомментируйте ее, но закомментируйте следующую
			get_the_title(wc_get_page_id('shop')), // Запрашиваем название страницы магазина
			get_permalink(wc_get_page_id('shop')) // Запрашиваем URL страницы магазина
		];
		array_splice($crumbs, 1, 0, [$new_breadcrumb]); // Добавляем новый пункт в хлебные крошки  после пункта "Главная"
	}
	return $crumbs;
}, 10, 2 );
/**
 * ------------------------------------------------------------------------------------------------
 * Load all CORE Classes and files
 * ------------------------------------------------------------------------------------------------
 */

if ( ! function_exists( 'woodmart_load_classes' ) ) {
	function woodmart_load_classes() {
		$classes = array(
			'Singleton.php',
			'Ajaxresponse.php',
			'Api.php',
			'Googlefonts.php',
			'Config.php',
			'Cssparser.php',
			'Layout.php',
			'License.php',
			'Notices.php',
			'Options.php',
			'Stylesstorage.php',
			'Theme.php',
			'Themesettingscss.php',
			'Vctemplates.php',
			'Wpbcssgenerator.php',
			'Registry.php',
			'Pagecssfiles.php',
		);

		foreach ( $classes as $class ) {
			require WOODMART_CLASSES . DIRECTORY_SEPARATOR . $class;
		}
	}
}

woodmart_load_classes();

new WOODMART_Theme();
