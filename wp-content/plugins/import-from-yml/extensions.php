<?php if ( ! defined('ABSPATH') ) { exit; }
function ipytw_extensions_page() { ?>
 <style>.button-primary {text-align: center; margin: 0 auto !important;} .ipytw_banner {max-width: 100%}</style>
 <div id="ipytw_extensions" class="wrap"> 
  	<!-- h1 style="font-size: 32px; text-align: center; color: #5b2942;"><?php _e('Extensions for', 'ipytw'); ?> Import from YML</h1 --> 
  	<div id="dashboard-widgets-wrap"><div id="dashboard-widgets" class="metabox-holder">	
		<div id="postbox-container-1"><div class="meta-box-sortables">
			<div class="postbox">
				<a href="https://icopydoc.ru/product/import-from-yml-pro/?utm_source=import-from-yml&utm_medium=organic&utm_campaign=in-plugin-import-from-yml&utm_content=extensions&utm_term=banner-yml-pro" target="_blank"><img class="ipytw_banner" src="<?php echo IPYTW_PLUGIN_DIR_URL; ?>/img/import-from-yml-pro-banner.jpg" alt="Upgrade to Import from YML Pro" /></a>
				<div class="inside">
					<table class="form-table"><tbody>
						<tr>
							<td class="overalldesc" style="font-size: 20px;">
								<h1 style="font-size: 24px; text-align: center; color: #5b2942;">Import from YML Pro</h1>
								<!-- img style="max-width: 100%;" src="<?php echo IPYTW_PLUGIN_DIR_URL; ?>/img/ex1.jpg" alt="img" / -->
								<ul style="text-align: center;">
									<li>&#10004; <?php _e('Ability to import products from certain categories', 'ipytw'); ?>;</li>
									<li>&#10004; <?php _e('Ability to import all images', 'ipytw'); ?>;</li>
									<li>&#10004; <?php _e('Ability to make a mark-up on products', 'ipytw'); ?>;</li>
									<li>&#10004; <?php _e('Ability to import data on stock', 'ipytw'); ?>;</li>
								</ul>
								<p style="text-align: center;"><a class="button-primary" href="https://icopydoc.ru/product/import-from-yml-pro/?utm_source=import-from-yml&utm_medium=organic&utm_campaign=in-plugin-import-from-yml&utm_content=extensions&utm_term=poluchit-yml-pro" target="_blank"><?php _e('Get', 'ipytw'); ?> Import from YML Pro <?php _e('Now', 'ipytw'); ?></a><br /></p>
							</td>
						</tr>
					</tbody></table>
				</div>
			</div>
		</div></div>	
  	</div></div>
 </div>
<?php
} /* end функция расширений ipytw_extensions_page */
?>