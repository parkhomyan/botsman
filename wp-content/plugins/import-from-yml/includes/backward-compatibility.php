<?php if (!defined('ABSPATH')) {exit;}
/*
Version: 1.0.0
Date: 04-01-2022
Author: Maxim Glazunov
Author URI: https://icopydoc.ru 
License: GPLv2
Description: This code helps ensure backward compatibility with older versions of the plugin.
*/

/*
* @since 1.0.0
*
* @return string/NULL
*
* Возвращает версию Woocommerce
*/ 
define('ipytw_VER', '1.6.0'); // для совместимости со старыми прошками
/*
* @since 1.0.0
*
* @param string $text (require)
*
* @return nothing
* Записывает файл логов /wp-content/uploads/import-from-yml/ipytw.log
*/
function ipytw_error_log($text) {	
	$ipytw_keeplogs = ipytw_optionGET('ipytw_keeplogs');

	if ($ipytw_keeplogs !== 'on') {return;}
	$upload_dir = (object)wp_get_upload_dir();
	$name_dir = $upload_dir->basedir."/import-from-yml";
	// подготовим массив для записи в файл логов
	if (is_array($text)) {$r = ipytw_array_to_log($text); unset($text); $text = $r;}
	if (is_dir($name_dir)) {
		$filename = $name_dir.'/ipytw.log';
		file_put_contents($filename, '['.date('Y-m-d H:i:s').'] '.$text.PHP_EOL, FILE_APPEND);		
	} else {
		if (!mkdir($name_dir)) {
			error_log('Нет папки import-from-yml! И создать не вышло! $name_dir ='.$name_dir.'; Файл: functions.php; Строка: '.__LINE__, 0);
		} else {
			error_log('Создали папку ipytw!; Файл: functions.php; Строка: '.__LINE__, 0);
			$filename = $name_dir.'/ipytw.log';
			file_put_contents($filename, '['.date('Y-m-d H:i:s').'] '.$text.PHP_EOL, FILE_APPEND);
		}
	} 
	return;
}
/*
* @since 1.0.0
*
* @param string $text (require)
* @param int $i (not require)
* @param string $res (not require)
*
* @return formatted string
* Позволяте писать в логи массив /wp-content/uploads/import-from-yml/ipytw.log
*/
function ipytw_array_to_log($text, $i=0, $res = '') {
	$tab = ''; for ($x = 0; $x<$i; $x++) {$tab = '---'.$tab;}
	if (is_array($text)) { 
	$i++;
	foreach ($text as $key => $value) {
		if (is_array($value)) {	// массив
			$res .= PHP_EOL .$tab."[$key] => ";
			$res .= $tab.ipytw_array_to_log($value, $i);
		} else { // не массив
			$res .= PHP_EOL .$tab."[$key] => ". $value;
		}
	}
	} else {
		$res .= PHP_EOL .$tab.$text;
	}
	return $res;
}
function ipytw_del_feed_zero() {
	$feedId = '0';
	$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
	unset($ipytw_settings_arr[$feedId]);
	wp_clear_scheduled_hook('ipytw_cron_period', array($feedId)); // отключаем крон
	wp_clear_scheduled_hook('ipytw_cron_sborki', array($feedId)); // отключаем крон
	$upload_dir = (object)wp_get_upload_dir();
	$name_dir = $upload_dir->basedir."/import-from-yml";
	$filename = $name_dir.'/feed-imported-offers-'.$feedId.'.tmp'; if (file_exists($filename)) {unlink($filename);}
	$filename = $name_dir.'/feed-importetd-cat-ids-'.$feedId.'.tmp'; if (file_exists($filename)) {unlink($filename);}
	ipytw_optionUPD('ipytw_settings_arr', $ipytw_settings_arr);
	ipytw_optionDEL('ipytw_status_sborki', $feedId);
	ipytw_optionDEL('ipytw_last_element', $feedId);

	$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
	for ($n = 1; $n < count($ipytw_registered_feeds_arr); $n++) { // первый элемент не проверяем, тк. там инфо по последнему id
		if ($ipytw_registered_feeds_arr[$n]['id'] === $feedId) {
			unset($ipytw_registered_feeds_arr[$n]);
			$ipytw_registered_feeds_arr = array_values($ipytw_registered_feeds_arr); 
			ipytw_optionUPD('ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
			break;
		}
	}

	$feedId = get_first_feed_id();
}
?>