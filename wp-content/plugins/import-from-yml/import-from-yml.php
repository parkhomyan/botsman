<?php if (!defined('ABSPATH')) {exit;}
/**
* Plugin Name: Import from YML
* Plugin URI: https://icopydoc.ru/category/documentation/import-from-yml/ 
* Description: Import products from YML to WooCommerce
* Version: 1.6.4
* Requires at least: 4.5
* Requires PHP: 5.6
* Author: Maxim Glazunov
* Author URI: https://icopydoc.ru
* License: GPL v2 or later
* License URI: https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: import-from-yml
* Domain Path: /languages
* Tags: yml, yandex, import, export, woocommerce
* WC requires at least: 3.0.0
* WC tested up to: 6.1.1
*/
/*	Copyright YEAR  PLUGIN_AUTHOR_NAME (email : djdiplomat@yandex.ru)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2, as
	published by the Free Software Foundation.
 
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
 
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/
// Check if WooCommerce is active
$plugin = 'woocommerce/woocommerce.php';
if (!in_array($plugin, apply_filters('active_plugins', get_option('active_plugins', array()))) && !(is_multisite() && array_key_exists($plugin, get_site_option('active_sitewide_plugins', array())))) {
	add_action('admin_notices', 'ipytw_warning_notice');
	return;
}
/**
* Display a notice in the admin Plugins page if the plugin is activated while WooCommerce is deactivated.
*
* @hook admin_notices
* @since 1.0.0
*/
function ipytw_warning_notice() {
	$class = 'notice notice-error';
	$message = 'Import from YML '. __('requires WooCommerce installed and activated', 'ipytw');
	printf('<div class="%1$s"><p>%2$s</p></div>', $class, $message);
}
// end Check if WooCommerce is active

$upload_dir = wp_get_upload_dir();
define('IPYTW_SITE_UPLOADS_URL', $upload_dir['baseurl']); // http://site.ru/wp-content/uploads
define('IPYTW_SITE_UPLOADS_DIR_PATH', $upload_dir['basedir']); // /home/site.ru/public_html/wp-content/uploads

define('IPYTW_PLUGIN_VERSION', '1.6.4'); // 1.0.0
define('IPYTW_PLUGIN_UPLOADS_DIR_URL', $upload_dir['baseurl'].'/import-from-yml'); // http://site.ru/wp-content/uploads/import-from-yml
define('IPYTW_PLUGIN_UPLOADS_DIR_PATH', $upload_dir['basedir'].'/import-from-yml'); // /home/site.ru/public_html/wp-content/uploads/import-from-yml
define('IPYTW_PLUGIN_DIR_URL', plugin_dir_url(__FILE__)); // http://site.ru/wp-content/plugins/import-from-yml/
define('IPYTW_PLUGIN_DIR_PATH', plugin_dir_path(__FILE__)); // /home/p135/www/site.ru/wp-content/plugins/import-from-yml/
define('IPYTW_PLUGIN_MAIN_FILE_PATH', __FILE__); // /home/p135/www/site.ru/wp-content/plugins/import-from-yml/import-from-yml.php
define('IPYTW_PLUGIN_SLUG', wp_basename(dirname(__FILE__))); // import-from-yml - псевдоним плагина
define('IPYTW_PLUGIN_BASENAME', plugin_basename(__FILE__)); // import-from-yml/import-from-yml.php - полный псевдоним плагина (папка плагина + имя главного файла)
unset($upload_dir);

require_once IPYTW_PLUGIN_DIR_PATH.'/packages.php';
register_activation_hook(__FILE__, array('ImportProductsYMLtoWooCommerce', 'on_activation'));
register_deactivation_hook(__FILE__, array('ImportProductsYMLtoWooCommerce', 'on_deactivation'));
add_action('plugins_loaded', array('ImportProductsYMLtoWooCommerce', 'init')); // активируем плагин

final class ImportProductsYMLtoWooCommerce {
	private $site_uploads_url = IPYTW_SITE_UPLOADS_URL; // http://site.ru/wp-content/uploads
	private $site_uploads_dir_path = IPYTW_SITE_UPLOADS_DIR_PATH; // /home/site.ru/public_html/wp-content/uploads
	private $plugin_version = IPYTW_PLUGIN_VERSION; // 1.0.0
	private $plugin_upload_dir_url = IPYTW_PLUGIN_UPLOADS_DIR_URL; // http://site.ru/wp-content/uploads/import-from-yml/
	private $plugin_upload_dir_path = IPYTW_PLUGIN_UPLOADS_DIR_PATH; // /home/site.ru/public_html/wp-content/uploads/import-from-yml/
	private $plugin_dir_url = IPYTW_PLUGIN_DIR_URL; // http://site.ru/wp-content/plugins/import-from-yml/
	private $plugin_dir_path = IPYTW_PLUGIN_DIR_PATH; // /home/p135/www/site.ru/wp-content/plugins/import-from-yml/
	private $plugin_main_file_path = IPYTW_PLUGIN_MAIN_FILE_PATH; // /home/p135/www/site.ru/wp-content/plugins/import-from-yml/import-from-yml.php
	private $plugin_slug = IPYTW_PLUGIN_SLUG; // import-from-yml - псевдоним плагина
	private $plugin_basename = IPYTW_PLUGIN_BASENAME; // import-from-yml/import-from-yml.php - полный псевдоним плагина (папка плагина + имя главного файла)

	protected static $instance;
	public static function init() {
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;  
	}

	public static function on_activation() {
		if (!current_user_can('activate_plugins')) {return;}

		$name_dir = IPYTW_SITE_UPLOADS_DIR_PATH.'/import-from-yml';
		if (!is_dir($name_dir)) {
			if (!mkdir($name_dir)) {
				error_log('ERROR: Ошибка создания папки '.$name_dir.'; Файл: import-from-yml.php; Строка: '.__LINE__, 0);
			}
		}

		$ipytw_registered_feeds_arr = array(
			0 => array('last_id' => '1'),
			1 => array('id' => '1')
		);

		$def_plugin_date_arr = new IPYTW_Data_Arr();
		$ipytw_settings_arr = array();
		$ipytw_settings_arr['1'] = $def_plugin_date_arr->get_opts_name_and_def_date('all');

		if (is_multisite()) {
			add_blog_option(get_current_blog_id(), 'ipytw_keeplogs', '0');
			add_blog_option(get_current_blog_id(), 'ipytw_enable_backend_debug', '0');
			add_blog_option(get_current_blog_id(), 'ipytw_version', '1.6.4');
			add_blog_option(get_current_blog_id(), 'ipytw_disable_notices', '0');
			add_blog_option(get_current_blog_id(), 'ipytw_enable_five_min', '0');
			add_blog_option(get_current_blog_id(), 'ipytw_errors', '');
			add_blog_option(get_current_blog_id(), 'ipytw_settings_arr', $ipytw_settings_arr);
			add_blog_option(get_current_blog_id(), 'ipytw_status_sborki1', '-1');
			add_blog_option(get_current_blog_id(), 'ipytw_last_element1', '-1');
			add_blog_option(get_current_blog_id(), 'ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
		} else {
			add_option('ipytw_keeplogs', '0');
			add_option('ipytw_enable_backend_debug', '0');
			add_option('ipytw_version', '1.6.4', '', 'no'); // без автозагрузки
			add_option('ipytw_disable_notices', '0', '', 'no');
			add_option('ipytw_enable_five_min', '0', '', 'no');
			add_option('ipytw_errors', '', '', 'no');
			add_option('ipytw_settings_arr', $ipytw_settings_arr);
			add_option('ipytw_status_sborki1', '-1');
			add_option('ipytw_last_element1', '-1');
			add_option('ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
		}
	}
	
	public static function on_deactivation() {
		if (!current_user_can('activate_plugins')) {return;}

		$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
		for ($i = 1; $i < count($ipytw_registered_feeds_arr); $i++) { // с единицы, т.к инфа по конкретным фидам там
			$feedId = $ipytw_registered_feeds_arr[$i]['id'];
			wp_clear_scheduled_hook('ipytw_cron_period', array($feedId)); // отключаем крон
			wp_clear_scheduled_hook('ipytw_cron_sborki', array($feedId)); // отключаем крон
		}
	}

	public function __construct() {
		load_plugin_textdomain('ipytw', false, $this->plugin_slug.'/languages/'); // load translation
		$this->check_options_upd(); // проверим, нужны ли обновления опций плагина
		$this->init_hooks(); // подключим хуки
 	}

	public function init_hooks() {
		add_action('admin_init', array($this, 'listen_submits_func'), 10);
		add_action('admin_init', array($this, 'admin_style_func'), 9999);
		add_action('admin_menu', array($this, 'add_admin_menu_func'), 10, 1);
		add_action('admin_notices', array($this, 'admin_notices_func'), 10, 1);

		add_action('ipytw_cron_sborki', array($this, 'ipytw_do_this_fifty_sec'), 10, 1);
		add_action('ipytw_cron_period', array($this, 'ipytw_do_this_event'), 10, 1);
		
		add_filter('plugin_action_links', array($this, 'plugin_action_links_func'), 10, 2);
		add_filter('upload_mimes', array($this, 'add_mime_types_func'), 99); // чутка позже остальных
		add_filter('cron_schedules', array($this, 'add_cron_intervals_func'), 10, 1);
	}

	public function check_options_upd() {
		$plugin_version = $this->get_plugin_version();
		if ($plugin_version == false) { // вероятно, у нас первичная установка плагина
			if (is_multisite()) {
				update_blog_option(get_current_blog_id(), 'ipytw_version', IPYTW_PLUGIN_VERSION);
			} else {
				update_option('ipytw_version', IPYTW_PLUGIN_VERSION);
			}
		} else if ($plugin_version !== $this->plugin_version) {
			add_action('init', array($this, 'set_new_options'), 10); // автообновим настройки, если нужно
		}
	}

	public function get_plugin_version() {
		if (is_multisite()) {
			$v = get_blog_option(get_current_blog_id(), 'ipytw_version');
		} else {
			$v = get_option('ipytw_version');
		}
		return $v;
	}

	public function set_new_options() {
/*		wp_clean_plugins_cache();
		wp_clean_update_cache();
		add_filter('pre_site_transient_update_plugins', '__return_null');
		wp_update_plugins();
		remove_filter('pre_site_transient_update_plugins', '__return_null');
*/
		/* Если версия плагина ниже 1.3.0 */
		$ipytw_version = ipytw_optionGET('ipytw_version');		
		if (version_compare($ipytw_version, '1.3.0', '<')) { 
			$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
			$ipytw_settings_arr_keys_arr = array_keys($ipytw_settings_arr);
			for ($i = 0; $i<count($ipytw_settings_arr_keys_arr); $i++) {
				$key = $ipytw_settings_arr_keys_arr[$i];
				if (isset($ipytw_settings_arr[$key])) {
					$newkey = (string)$key; // преобразовываем ключ массива из числовго в строковый
					$ipytw_settings_arr_keys_arr[$newkey] = $ipytw_settings_arr_keys_arr[$i];
					unset($ipytw_settings_arr_keys_arr[$i]); // удаляем числовой ключ
					$feedId = $key;
					wp_clear_scheduled_hook('ipytw_cron_period', array($feedId)); // отключаем крон
					wp_clear_scheduled_hook('ipytw_cron_sborki', array($feedId)); // отключаем крон
	
					$ipytw_status_sborki_key = 'ipytw_status_sborki'.$key;
					$ipytw_last_element_key = 'ipytw_last_element'.$key;
					ipytw_optionDEL($ipytw_status_sborki_key, $key);
					ipytw_optionDEL($ipytw_last_element_key, $key);
					ipytw_optionADD($ipytw_status_sborki_key, '-1');
					ipytw_optionADD($ipytw_last_element_key, '-1');
				}
			}
			ipytw_optionUPD('ipytw_settings_arr', $ipytw_settings_arr);
	
			$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
			for ($i = 1; $i < count($ipytw_registered_feeds_arr); $i++) { // с единицы, т.к инфа по конкретным фидам там
				$feedId = $ipytw_registered_feeds_arr[$i]['id'];
				$ipytw_registered_feeds_arr[$i]['id'] = (string)$feedId;
			}	
			ipytw_optionUPD('ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
		}
		/* end Если версия плагина ниже 1.3.0 */
	
		// получим список дефолтных настроек
		$ipytw_data_arr_obj = new IPYTW_Data_Arr();
		$opts_arr = $ipytw_data_arr_obj->get_opts_name_and_def_date_obj('all'); // список дефолтных настроек
		// проверим, заданы ли дефолтные настройки
		$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
		$ipytw_settings_arr_keys_arr = array_keys($ipytw_settings_arr);
		for ($i = 0; $i < count($ipytw_settings_arr_keys_arr); $i++) {
			$feedId = (string)$ipytw_settings_arr_keys_arr[$i];
			for ($n = 0; $n < count($opts_arr); $n++) {
				$name = $opts_arr[$n]->get_name();
				$value = $opts_arr[$n]->get_value();	
				if (!isset($ipytw_settings_arr[$feedId][$name])) {ipytw_optionUPD($name, $value, $feedId, 'yes', 'set_arr');}
			}
		}

		if (is_multisite()) {
			update_blog_option(get_current_blog_id(), 'ipytw_version', IPYTW_PLUGIN_VERSION);
		} else {
			update_option('ipytw_version', IPYTW_PLUGIN_VERSION);
		}
	}

	public function listen_submits_func() {
		do_action('ipytw_listen_submits');
	}

	public function admin_style_func() {
		wp_register_style('ipytw-admin-css', $this->plugin_dir_url.'css/ipytw_style.css');
	}

	// Добавляем пункты меню
	public function add_admin_menu_func() {
		$page_suffix = add_menu_page(null , __('Import from YML', 'ipytw'), 'manage_options', 'ipytwexport', 'ipytw_export_page', 'dashicons-redo', 51);
		require_once IPYTW_PLUGIN_DIR_PATH.'/export.php'; // Подключаем файл настроек
		// создаём хук, чтобы стили выводились только на странице настроек
		add_action('admin_print_styles-'. $page_suffix, array($this, 'admin_css_func'));
		add_action('admin_print_styles-'. $page_suffix, array($this, 'admin_head_css_func'));

		add_submenu_page('ipytwexport', __('Debug', 'ipytw'), __('Debug page', 'ipytw'), 'manage_options', 'ipytwdebug', 'ipytw_debug_page');
		require_once IPYTW_PLUGIN_DIR_PATH.'/debug.php';
		$page_subsuffix = add_submenu_page('ipytwexport', __('Add Extensions', 'ipytw'), __('Extensions', 'ipytw'), 'manage_options', 'ipytwextensions', 'ipytw_extensions_page');
		require_once IPYTW_PLUGIN_DIR_PATH.'/extensions.php';
		add_action('admin_print_styles-'. $page_subsuffix, array($this, 'admin_css_func'));
	} 

	// Вывод различных notices
	public function admin_notices_func() {
		$ipytw_enable_backend_debug = ipytw_optionGET('ipytw_enable_backend_debug'); 
		if ($ipytw_enable_backend_debug === 'on') {
			print '<div class="updated notice notice-success is-dismissible"><p>';
			print '<h1>Зарегистрированные фиды:</h1>';
			$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
			echo get_array_as_string($ipytw_registered_feeds_arr, '<br/>');
	
			print '<h1>Динамические опции:</h1>';
			$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
			echo get_array_as_string($ipytw_settings_arr, '<br/>');
	
			print '<h1>Статусы сборок:</h1>';
			for ($i = 1; $i < count($ipytw_registered_feeds_arr); $i++) { // с единицы, т.к инфа по конкретным фидам там
				$feedId = $ipytw_registered_feeds_arr[$i]['id'];
				$ipytw_status_sborki_val = ipytw_optionGET('ipytw_status_sborki', $feedId);
				$ipytw_last_element_val = ipytw_optionGET('ipytw_last_element', $feedId);
				if ($feedId === '0' || $feedId === 0) {$feedId = '';}
				$ipytw_status_sborki_key = 'ipytw_status_sborki'.$feedId;
				$ipytw_last_element_key = 'ipytw_last_element'.$feedId;
				print '$ipytw_status_sborki_key = '.$ipytw_status_sborki_key.'; $ipytw_status_sborki_val = '.$ipytw_status_sborki_val.'<br/>'; 
				print '$ipytw_last_element_key = '.$ipytw_last_element_key.'; $ipytw_last_element_val = '.$ipytw_last_element_val.'<br/>';
			}
			print '<h1>Крон задачи (ipytw_cron_period) и (ipytw_cron_sborki):</h1>';
			$cron_zadachi = get_option('cron'); // получаем все задачи из базы данных
			echo get_array_as_string($cron_zadachi, '<br/>'); // можно использовать функции print_r() или var_dump() для вывода всех задач
			print '</p></div>';
		}

		if (isset($_REQUEST['ipytw_submit_action'])) {
			$run_text = '';
			if (sanitize_text_field($_POST['ipytw_run_cron']) !== 'off') {
				$run_text = '. '. __('Import products is running. You can continue working with the website', 'ipytw');
			}
			print '<div class="updated notice notice-success is-dismissible"><p>'. __('Updated', 'ipytw'). $run_text .'.</p></div>';
		}
	
		if (isset($_REQUEST['ipytw_submit_debug_page'])) {
			print '<div class="updated notice notice-success is-dismissible"><p>'. __('Updated', 'ipytw'). '.</p></div>';
		}

		if (isset($_REQUEST['ipytw_submit_clear_logs'])) {
			$upload_dir = (object)wp_get_upload_dir();
			$name_dir = $upload_dir->basedir."/import-from-yml";
			$filename = $name_dir.'/plugin.log';
			if (file_exists($filename)) {$res = unlink($filename);} else {$res = false;}
			if ($res == true) {
				print '<div class="notice notice-success is-dismissible"><p>' .__('Logs were cleared', 'ipytw'). '.</p></div>';					
			} else {
				print '<div class="notice notice-warning is-dismissible"><p>' .__('Error accessing log file. The log file may have been deleted previously', 'ipytw'). '.</p></div>';		
			}
		}

		$ipytw_disable_notices = ipytw_optionGET('ipytw_disable_notices');
		if ($ipytw_disable_notices === 'on') {} else {
			$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
			for ($i = 1; $i < count($ipytw_registered_feeds_arr); $i++) { // с единицы, т.к инфа по конкретным фидам там
				$feedId = $ipytw_registered_feeds_arr[$i]['id'];		
				$status_sborki = ipytw_optionGET('ipytw_status_sborki', $feedId); // ожидается тип (int)
				switch ($status_sborki) {
					case 1:
						print '<div class="updated notice notice-success is-dismissible"><p>'. __('Feed ID', 'ipytw').' = '.$feedId.'. '. __('Import products is running', 'ipytw').'. '. __('Step', 'ipytw').': 1. '. __('Getting a feed from the source site', 'ipytw').'.</p></div>';
					break;
					case 2:
						print '<div class="updated notice notice-success is-dismissible"><p>'. __('Feed ID', 'ipytw').' = '.$feedId.'. '. __('Import products is running', 'ipytw').'. '. __('Step', 'ipytw').': 2. '. __('Importing a list of categories', 'ipytw').'.</p></div>';
					break;
					case 3:
						$ipytw_last_element = (int)ipytw_optionGET('ipytw_last_element', $feedId) + 1;
						$ipytw_count_elements = ipytw_optionGET('ipytw_count_elements', $feedId, 'set_arr');
						if ($ipytw_count_elements == -1) {$ipytw_count_elements = '('. __('products are being counted', 'ipytw').'...)';}
						print '<div class="updated notice notice-success is-dismissible"><p>'. __('Feed ID', 'ipytw').' = '.$feedId.'. '. __('Import products is running', 'ipytw').'. '. __('Step', 'ipytw').': 3. '. __('Processed products', 'ipytw').': '.$ipytw_last_element.' '. __('of', 'ipytw').' '.$ipytw_count_elements.'.</p></div>';
					break;
					default:
				}
			}
		}

		/* отправка отчёта */
		if (isset($_REQUEST['ipytw_submit_send_stat'])) {
			if (!empty($_POST) && check_admin_referer('ipytw_nonce_action_send_stat', 'ipytw_nonce_field_send_stat')) { 	
			if (is_multisite()) { 
				$ipytw_is_multisite = 'включен';	
				$ipytw_keeplogs = get_blog_option(get_current_blog_id(), 'ipytw_keeplogs');
			} else {
				$ipytw_is_multisite = 'отключен'; 
				$ipytw_keeplogs = get_option('ipytw_keeplogs');
			}

			$unixtime = current_time('Y-m-d H:i');
			$mail_content = '<h1>Заявка (#'.$unixtime.')</h1>';
			$mail_content .= "Версия плагина: ". IPYTW_PLUGIN_VERSION . "<br />";
			$mail_content .= "Версия WP: ".get_bloginfo('version'). "<br />";	 
			$woo_version = get_woo_version_number();
			$mail_content .= "Версия WC: ".$woo_version. "<br />";
			$mail_content .= "Версия PHP: ".phpversion(). "<br />";   
			$mail_content .= "Режим мультисайта: ".$ipytw_is_multisite. "<br />";
			$mail_content .= "Вести логи: ".$ipytw_keeplogs. "<br />";
			$upload_dir = wp_get_upload_dir();
			$mail_content .= 'Расположение логов: <a href="'.$upload_dir['baseurl'].'/import-from-yml/plugin.log" target="_blank">'.$upload_dir['basedir'].'/import-from-yml/plugin.log</a><br />';		
			$possible_problems_arr = ipytw_possible_problems_list();
			if ($possible_problems_arr[1] > 0) {
				$possible_problems_arr[3] = str_replace('<br/>', PHP_EOL, $possible_problems_arr[3]);
				$mail_content .= "Самодиагностика: <br/>".$possible_problems_arr[3];
			} else {
				$mail_content .= "Самодиагностика: Функции самодиагностики не выявили потенциальных проблем". "<br />";
			}
			if (!class_exists('ImportProductsYMLtoWooCommercePro')) {
				$mail_content .= "Pro: не активна". "<br />";
			} else {
				if (!defined('ipytwp_VER')) {define('ipytwp_VER', 'н/д');} 
				$order_id = ipytw_optionGET('ipytwp_order_id');
				$order_email = ipytw_optionGET('ipytwp_order_email');			
				$mail_content .= "Pro: активна (v ".ipytwp_VER." (#".$order_id." / ".$order_email."))". "<br />";
			}
			if (isset($_REQUEST['ipytw_its_ok'])) {
				$mail_content .= "<br />Помог ли плагин: ".sanitize_text_field($_REQUEST['ipytw_its_ok']);
			}
			if (isset($_POST['ipytw_email'])) {				
				$mail_content .= '<br />Почта: <a href="mailto:'.sanitize_email($_POST['ipytw_email']).'?subject=Ответ разработчика Import from YML (#'.$unixtime.')" target="_blank" rel="nofollow noreferer" title="'.sanitize_email($_POST['ipytw_email']).'">'.sanitize_email($_POST['ipytw_email']).'</a>';
			}
			if (isset($_POST['ipytw_message'])) {
				$mail_content .= "<br />Сообщение: ".sanitize_text_field($_POST['ipytw_message']). "<br />";
			}

			$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
			for ($i = 1; $i < count($ipytw_registered_feeds_arr); $i++) { // с единицы, т.к инфа по конкретным фидам там
				$feedId = $ipytw_registered_feeds_arr[$i]['id'];

				$ipytw_url_yml_file = ipytw_optionGET('ipytw_url_yml_file', $feedId, 'set_arr');
				$ipytw_date_sborki = ipytw_optionGET('ipytw_date_sborki', $feedId, 'set_arr');
				$ipytw_errors = ipytw_optionGET('ipytw_errors', $feedId, 'set_arr');

				$mail_content .= "<br />ФИД №: ".$feedId. "<br />";
				$mail_content .= "УРЛ: ".get_site_url(). "<br />";
				$mail_content .= "УРЛ YML-фида: ".$ipytw_url_yml_file . "<br />";
				$mail_content .= "Дата последнего импорта XML: ".$ipytw_date_sborki. "<br />";
				$mail_content .= "Ошибки: ".$ipytw_errors. "<br />";
			}

			add_filter('wp_mail_content_type', array($this, 'set_html_content_type'));
			wp_mail('support@icopydoc.ru', 'Отчёт Import from YML', $mail_content);
			// Сбросим content-type, чтобы избежать возможного конфликта
			remove_filter('wp_mail_content_type', array($this, 'set_html_content_type'));

			print '<div class="updated notice notice-success is-dismissible"><p>'. __('The data has been sent. Thank you', 'ipytw'). '.</p></div>';
			}
		} /* end отправка отчёта */ 
	}
	public static function set_html_content_type() {
		return 'text/html';
	}

	public static function admin_css_func() { // Ставим css-файл в очередь на вывод	
		wp_enqueue_style('ipytw-admin-css'); 
	}

	/* Фильтры */

	public function plugin_action_links_func($actions, $plugin_file) {
		if (false === strpos($plugin_file, basename(__FILE__))) { // проверка, что у нас текущий плагин
			return $actions;
		}
		$settings_link = '<a style="color: green; font-weight: 700;" href="/wp-admin/admin.php?page=ipytwextensions">'. __('More features', 'ipytw').'</a>';
		array_unshift($actions, $settings_link); 	
		$settings_link = '<a href="/wp-admin/admin.php?page=ipytwexport">'. __('Settings', 'ipytw').'</a>';
		array_unshift($actions, $settings_link); 
		$actions = apply_filters('ipytw_plugin_action_links_after', $actions, $settings_link, $plugin_file);
		return $actions; 
	}

	public function admin_head_css_func() {
		/* печатаем css в шапке админки */
		print '<style>/* Import from YML */
			.metabox-holder .postbox-container .empty-container {height: auto !important;}
			.icp_img1 {background-image: url('. $this->plugin_dir_url .'img/sl1.jpg);}
			.icp_img2 {background-image: url('. $this->plugin_dir_url .'img/sl2.jpg);}
			.icp_img3 {background-image: url('. $this->plugin_dir_url .'img/sl3.jpg);}
			.icp_img4 {background-image: url('. $this->plugin_dir_url .'img/sl4.jpg);}
			.icp_img5 {background-image: url('. $this->plugin_dir_url .'img/sl5.jpg);}
			.icp_img6 {background-image: url('. $this->plugin_dir_url .'img/sl6.jpg);}
			.icp_img7 {background-image: url('. $this->plugin_dir_url .'img/sl7.jpg);}
		</style>';
	}
 
	// Разрешим загрузку xml и csv файлов
	public function add_mime_types_func($mimes) {
		$mimes ['csv'] = 'text/csv';
		$mimes ['xml'] = 'text/xml';
		$mimes ['yml'] = 'text/xml';
		return $mimes;
	} 

	// добавляем интервалы крон в 70 секунд и 6 часов 
	public function add_cron_intervals_func($schedules) {
		$schedules['fifty_sec'] = array(
			'interval' => 61, // 50
			'display' => '61 sec'
		);
		$schedules['five_min'] = array(
			'interval' => 300,
			'display' => '5 min'
		);
		$schedules['six_hours'] = array(
			'interval' => 21600,
			'display' => '6 hours'
		);
		$schedules['every_two_days'] = array(
			'interval' => 172800,
			'display' => __('Every two days', 'ipytw')
		);
		return $schedules;
	}

	/* функции крона */
	public function ipytw_do_this_event($feedId) {
		new IPYTW_Error_Log('FEED № '.$feedId.'; Крон ipytw_do_this_event включен. Делаем что-то каждый час; Файл: import-from-yml.php; Строка: '.__LINE__);

		ipytw_optionUPD('ipytw_status_sborki', 1, $feedId);
		ipytw_optionUPD('ipytw_last_element', 0, $feedId); // выставляем в положения начала сборки

		wp_clear_scheduled_hook('ipytw_cron_sborki', array($feedId));

		// Возвращает nul/false. null когда планирование завершено. false в случае неудачи.
		$res = wp_schedule_event(time(), 'fifty_sec', 'ipytw_cron_sborki', array($feedId));
		if ($res === false) {
			new IPYTW_Error_Log('FEED № '.$feedId.'; ERROR: Не удалось запланировань CRON fifty_sec; Файл: import-from-yml.php; Строка: '.__LINE__);
		} else {
			new IPYTW_Error_Log('FEED № '.$feedId.'; CRON fifty_sec успешно запланирован; Файл: import-from-yml.php; Строка: '.__LINE__);
		}
	}

	public function ipytw_do_this_fifty_sec($feedId) {
		new IPYTW_Error_Log('FEED № '.$feedId.'; Крон ipytw_do_this_fifty_sec запущен; Файл: import-from-yml.php; Строка: '.__LINE__);
		$time_start_last_step = (int)current_time('timestamp', 1); // unixtime
		$this->ipytw_construct_yml($feedId, $time_start_last_step); // делаем что-либо каждые 50 сек
	}
 	/* end функции крона */

	// сборка
	public static function ipytw_construct_yml($feedId, $time_start_last_step) {
		new IPYTW_Error_Log('FEED № '.$feedId.'; Стартовала ipytw_construct_yml. Файл: import-from-yml.php; Строка: '.__LINE__);
		$status_sborki = (int)ipytw_optionGET('ipytw_status_sborki', $feedId);

		$ipytw_last_element = (int)ipytw_optionGET('ipytw_last_element', $feedId);
		$ipytw_step_import = ipytw_optionGET('ipytw_step_import', $feedId, 'set_arr');
		$timeout = $time_start_last_step + $ipytw_step_import - 2;

		$args_arr = array(
			'feed_id' => $feedId,
			'last_element' => $ipytw_last_element,
			'timeout' => $timeout
		);
		$import_obj = new IPYTW_Import($args_arr);

		switch ($status_sborki) {
			case 1: // загрузка файла с фидом на наш сервер
				new IPYTW_Error_Log('FEED № '.$feedId.'; Первый шаг. Файл: import-from-yml.php; Строка: '.__LINE__);

				$res_step = copy_feed_from_source($feedId);
				if ($res_step === false) {
					$import_obj->stop();				
					return;
				} else {
					ipytw_optionUPD('ipytw_status_sborki', 2, $feedId);
					return;
				}
			break;
			case 2: // создание категорий
				new IPYTW_Error_Log('FEED № '.$feedId.'; Второй шаг. Файл: import-from-yml.php; Строка: '.__LINE__);

				$import_obj->import_cat();
				ipytw_optionUPD('ipytw_status_sborki', 3, $feedId);
				return;
			break;
			case 3: // импорт товаров
				new IPYTW_Error_Log('FEED № '.$feedId.'; Третий шаг. Файл: import-from-yml.php; Строка: '.__LINE__);
		
				$ipytw_count_elements = (int)ipytw_optionGET('ipytw_count_elements', $feedId, 'set_arr');
				if ($ipytw_count_elements < 1) {
					if ($import_obj->set_count_elements() === true) {
						ipytw_optionUPD('ipytw_last_element', 0, $feedId); // также устанавливаем значение счётчика обработанных товаров
					} else {
						new IPYTW_Error_Log('FEED № '.$feedId.'; В фиде нет товаров! Останавливаем сборку; Файл: import-from-yml.php; Строка: '.__LINE__);
						$import_obj->stop();
						return;
					}
				}
				$ipytw_last_element = (int)ipytw_optionGET('ipytw_last_element', $feedId);

				$feed_imported_cat_arr = ipytw_get_imported_cat($feedId);
				new IPYTW_Error_Log('2 $ipytw_last_element = '.$ipytw_last_element.'; $ipytw_count_elements = '.$ipytw_count_elements.'' .__LINE__);
				if ($ipytw_last_element < $ipytw_count_elements-1) {
					new IPYTW_Error_Log('FEED № '.$feedId.'; $ipytw_last_element = '.$ipytw_last_element.' < $ipytw_count_elements = '.$ipytw_count_elements.'; Файл: import-from-yml.php; Строка: '.__LINE__);
					// ещё не все товары обработаны. Обработаем
					
					$xml_object = $import_obj->get_feed_our_site();
					$xml_offers_object = $xml_object->shop->offers;

					for ($i = $ipytw_last_element; $i < $ipytw_count_elements; $i++) {
						$offer_xml_object = $xml_offers_object->offer[$i];
						$imported_ids = ipytw_unit($offer_xml_object, $feed_imported_cat_arr, $feedId); // $feed_imported_offers_arr 

						if ($imported_ids === false) {
							new IPYTW_Error_Log('FEED № '.$feedId.'; ipytw_unit вернула false; Файл: import-from-yml.php; Строка: '.__LINE__);
						} else {
							new IPYTW_Error_Log('FEED № '.$feedId.'; ipytw_unit вернула $imported_ids; Файл: import-from-yml.php; Строка: '.__LINE__);
							new IPYTW_Error_Log($imported_ids);
							ipytw_wf($feedId, $imported_ids); // записываем кэш-файл
						}
						// ipytw_wf($feedId, $imported_ids); // записываем кэш-файл
						
						// https://wp-kama.ru/handbook/codex/object-cache
						wp_suspend_cache_addition(true);
						$status_sborki = ipytw_optionGET('ipytw_status_sborki', $feedId); // ещё раз проверим необходимость остановки всей сборки
						wp_suspend_cache_addition(false);
						if ($status_sborki == -1) { // останавливаем сборку
							$import_obj->stop();
							break;
						}
		
						ipytw_optionUPD('ipytw_last_element', $i, $feedId);
		
						$unixtime = (int)current_time('timestamp', 1);
						if ($unixtime >= $timeout) {
							new IPYTW_Error_Log('FEED № '.$feedId.'; $unixtime = '.$unixtime.' > $timeout = '.$timeout.'; Файл: import-from-yml.php; Строка: '.__LINE__);
							break;
						} else {
							new IPYTW_Error_Log('FEED № '.$feedId.'; $unixtime = '.$unixtime.' < $timeout = '.$timeout.'; Файл: import-from-yml.php; Строка: '.__LINE__);
						}
					}
				} else {
					$import_obj->stop();
				}
			break;
			default:
				new IPYTW_Error_Log('FEED № '.$feedId.'; Шаг default; $status_sborki = '.$status_sborki.'. Файл: import-from-yml.php; Строка: '.__LINE__);

				$import_obj->stop();
				return;
		}	
		return;
	} // end public static function ipytw_construct_yml
} /* end class ImportProductsYMLtoWooCommerce */
?>