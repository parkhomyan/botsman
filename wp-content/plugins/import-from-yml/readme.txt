﻿=== Import from YML ===
Contributors: icopydoc
Donate link: https://sobe.ru/na/import_from_yml
Tags: yml, yandex, import, export, woocommerce
Requires at least: 4.5
Tested up to: 5.9
Stable tag: 1.6.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Import products from YML-feed to WooCommerce.

== Description ==

Import products from YML-feed to WooCommerce.

The plugin Woocommerce is required!

PRO version: [https://icopydoc.ru/product/import-from-yml-pro/](https://icopydoc.ru/product/import-from-yml-pro/?utm_source=wp-repository&utm_medium=organic&utm_campaign=import-from-yml&utm_content=readme&utm_term=pro-version)

=== Purpose of the plugin ===

Import products from YML-feed to WooCommerce

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the entire `import-from-yml` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Go to the "Import from YML" page to configure the plugin

== Frequently Asked Questions ==

= What plugin online store supported by your plugin? =

Only Woocommerce.

== Screenshots ==

1. screenshot-1.png

== Changelog ==

= 1.6.4 =
* Fixed bugs
* Added import from the file
* Added the ability to import a product description into a short description

= 1.6.3 =
* Fixed bugs

= 1.6.2 =
* Fixed bugs

= 1.6.1 =
* Fixed bugs

= 1.6.0 =
* New plugin core

= 1.5.3 =
* Fixed bugs

= 1.5.2 =
* Added the ability to delete products if they are missing in the feed

= 1.5.1 =
* Fixed bugs

= 1.5.0 =
* Fixed bugs
* Added Pro version. See extensions page!

= 1.3.0 =
* New plugin core

= 1.2.3 =
* Fixed bug with stock status
* Fixed bugs

= 1.2.2 =
* Fixed bugs

= 1.2.1 =
* Fixed bug with categories when importing

= 1.2.0 =
* Fixed bugs
* Improthed sku import logic
* Added option not to import large image files
* Added the ability to partially update already imported products

= 1.1.2 =
* Fixed bugs
* Added the ability to import sku, sale price

= 1.1.1 =
* Fixed bugs
* Added the ability to import attributes

= 1.1.0 =
* Fixed bugs
* Added support for multiple feeds

= 1.0.2 =
* Fixed bugs

= 1.0.1 =
* Fixed bugs
* Added a feedback form 

= 1.0.0 =
* First relise

== Upgrade Notice ==

= 1.6.4 =
* Fixed bugs
* Added import from the file
* Added the ability to import a product description into a short description