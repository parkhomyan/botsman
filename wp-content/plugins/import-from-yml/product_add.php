<?php if (!defined('ABSPATH')) {exit;}
include_once ABSPATH . 'wp-admin/includes/plugin.php'; // без этого не будет работать вне адмники is_plugin_active
	function ipytw_product_add($data, $feed_id, $flag = false) {	
	new IPYTW_Error_Log('FEED № '.$feed_id.'; Стартовала ipytw_product_add; Файл: product_add.php; Строка: '.__LINE__);
	if (property_exists($data['offer_xml_object'], 'name')) {
		$product_name = (string)$data['offer_xml_object']->name;
	} else {
		new IPYTW_Error_Log('NOTICE: У товара нет названия! Пропускаем; Файл: product_add.php; Строка: '.__LINE__);
		return false;
	}

	$xml_offer_attr_object = $data['offer_xml_object']->attributes(); 
	if (property_exists($xml_offer_attr_object, 'available')) {
		$feed_product_available = (string)$xml_offer_attr_object->available;
		if ($feed_product_available === 'true') {$stock_status = 'instock';} else {$stock_status = 'outofstock';}
	} else {$stock_status = 'outofstock';}

	if (property_exists($data['offer_xml_object'], 'description')) {
		$product_description = (string)$data['offer_xml_object']->description;
	} else {$product_description = '';}
	$product_description = apply_filters('ipytw_product_description_filter', $product_description, array('data' => $data, 'flag' => $flag), $feed_id);

	if ($flag === false) {
		$product = new WC_Product();
	} else {
		$product = wc_get_product($flag);
	}
	$product->set_name($product_name);

	$ipytw_description_into = ipytw_optionGET('ipytw_description_into', $feed_id, 'set_arr');
	if ($ipytw_description_into === 'excerpt') {
		$product_description = apply_filters('ipytw_into_product_short_description_filter', $product_description, array('data' => $data, 'flag' => $flag), $feed_id);
		$product->set_short_description($product_description);
	} else {
		$product_description = apply_filters('ipytw_into_product_full_description_filter', $product_description, array('data' => $data, 'flag' => $flag), $feed_id);
		$product->set_description($product_description);
	}
	$product->set_stock_status($stock_status);
	// $product = apply_filters('ipytw_product_add_before_save_one_filter', $product, array('data' => $data, 'flag' => $flag), $feed_id);
	$site_product_id = $product->save();
	$product = wc_get_product($site_product_id);
	
	$unixtime = current_time('Y-m-d H:i');
	update_post_meta($site_product_id, '_ipytw_feed_product_id', $data['feed_product_id']); // id товара в фиде
	update_post_meta($site_product_id, '_ipytw_feed_id', $feed_id); // id фида
	update_post_meta($site_product_id, '_ipytw_date_last_import', $unixtime);

	// загрузка картинок
	$ipytw_product_was_sync = ipytw_optionGET('ipytw_product_was_sync', $feed_id, 'set_arr');
	new IPYTW_Error_Log('FEED № '.$feed_id.'; Приступаем к загрузке картинок; Файл: product_add.php; Строка: '.__LINE__);

	$img_ids_arr = array();
	foreach ($data['offer_xml_object']->picture as $pic) {
	//$feed_picture_url = (string)$data['offer_xml_object']->picture;
	$feed_picture_url = (string)$pic;
	$feed_picture_url = get_from_url($feed_picture_url);
	new IPYTW_Error_Log('FEED № '.$feed_id.'; url картинки = '.$feed_picture_url.'; Файл: product_add.php; Строка: '.__LINE__);
	$image_id = null;
	$check_sync_img = ipytw_check_sync($feed_picture_url, $feed_id, 'attachment'); /* ! */ // picture 
	if ($check_sync_img === false) {	
		$ipytw_fsize_limit = ipytw_optionGET('ipytw_fsize_limit', $feed_id, 'set_arr');
		$fsize_size = ipytw_fsize($feed_picture_url, 'MB', 'no', 0);
		if ($ipytw_fsize_limit > $fsize_size) { // размер файла в пределах лимита
			new IPYTW_Error_Log('FEED № '.$feed_id.'; $ipytw_fsize_limit = '.$ipytw_fsize_limit.' > $fsize_size = '.$fsize_size.'; Файл: product_add.php; Строка: '.__LINE__);
			$filename = basename(urldecode($feed_picture_url));
			if (!empty($filename)) {
				$upload_file = wp_upload_bits($filename, null, ipytw_file_get_contents_curl($feed_picture_url));
				if (!$upload_file['error']) {
					$wp_filetype = wp_check_filetype($filename, null);
					$attachment = array(
						'post_mime_type' => $wp_filetype['type'],
						'post_title' => $product_name,
						'post_content' => '',
						'post_status' => 'inherit'
					);
					$image_id = wp_insert_attachment($attachment, $upload_file['file']);
					new IPYTW_Error_Log('FEED № '.$feed_id.'; wp_insert_attachment =>; Файл: product_add.php; Строка: '.__LINE__);
					if (!is_wp_error($image_id)) {
						new IPYTW_Error_Log('FEED № '.$feed_id.'; $image_id = '.$image_id.'; Файл: product_add.php; Строка: '.__LINE__);
						require_once(ABSPATH . "wp-admin" . '/includes/image.php');
						$attachment_data = wp_generate_attachment_metadata($image_id, $upload_file['file']);
						wp_update_attachment_metadata($image_id, $attachment_data);
					} else {
						new IPYTW_Error_Log('FEED № '.$feed_id.'; ERROR: Ошибка при загрузке картинки; Файл: product_add.php; Строка: '.__LINE__);
						new IPYTW_Error_Log($image_id, 0);			
					}
					update_post_meta($image_id, '_ipytw_import_feed_picture_url', $feed_picture_url);
					if (!class_exists('ImportProductsYMLtoWooCommercePro')) {
					$product->set_image_id($image_id);
						break;} else {
						$img_ids_arr[] = $image_id; 
						do_action('ipytw_img_add', array('img_id' => $image_id, 'product' => $product));
					}
				}
			}
		} else { // размер файла в пределах лимита
			new IPYTW_Error_Log('FEED № '.$feed_id.'; Размер картинки выше лимита. Пропускаем картинку $ipytw_fsize_limit = '.$ipytw_fsize_limit.'; $fsize_size = '.$fsize_size.'; Файл: product_add.php; Строка: '.__LINE__);
		}
	} else {
		$image_id = $check_sync_img;
		if (!class_exists('ImportProductsYMLtoWooCommercePro')) {
		$product->set_image_id($image_id);
			break;} else {
			$img_ids_arr[] = $image_id; 
			do_action('ipytw_img_add', array('img_id' => $image_id, 'product' => $product));
		}
	}
	// $product->set_gallery_image_ids(array(1792));
	}
	do_action('ipytw_imgs_add', array('img_ids_arr' => $img_ids_arr, 'product' => $product));

	$site_product_cat = 0;
	$product_categoryId = $data['offer_xml_object']->categoryId;
	if ($product_categoryId !== '') { 
		$feed_product_cat = (int)$product_categoryId;
		new IPYTW_Error_Log('FEED № '.$feed_id.'; Пробуем отыскать категорию с $feed_product_cat = '.$feed_product_cat.'; Файл: product_add.php; Строка: '.__LINE__);
		if (isset($data['feed_imported_cat_arr'][$feed_product_cat])) {
			$site_product_cat = (int)$data['feed_imported_cat_arr'][$feed_product_cat];
			new IPYTW_Error_Log('FEED № '.$feed_id.'; Категории $feed_product_cat = '.$feed_product_cat.' соответсвует категория $site_product_cat = '.$site_product_cat.' на нашем сайте; Файл: product_add.php; Строка: '.__LINE__);
			$product->set_category_ids(array($site_product_cat));
		} else {
			new IPYTW_Error_Log('FEED № '.$feed_id.'; Соответсвия не нашлось. Продолжаем без категории; Файл: product_add.php; Строка: '.__LINE__);
		}
	}

	$price_element = 'price';
	$price_element = apply_filters('ipytw_price_element_filter', $price_element, $feed_id);
	$product_price = (float)$data['offer_xml_object']->$price_element;
	$product_price = apply_filters('ipytw_product_price_source_filter', $product_price, $data, $feed_id);
	$product_price = apply_filters('ipytw_product_price_filter', $product_price, $feed_id);
	$product->set_regular_price($product_price);

	$oldprice_element = 'oldprice';
	$oldprice_element = apply_filters('ipytw_oldprice_element_filter', $oldprice_element, $feed_id);
	$product_oldprice = (float)$data['offer_xml_object']->$oldprice_element;
	$product_oldprice = apply_filters('ipytw_product_oldprice_source_filter', $product_oldprice, $data, $feed_id);
	$product_oldprice = apply_filters('ipytw_product_oldprice_filter', $product_oldprice, $feed_id);
	if ($product_oldprice > (float)0) { 
		$product->set_sale_price($product_oldprice); 
	} else {
		$product->set_sale_price('');
	}

	$ipytw_source_sku = ipytw_optionGET('ipytw_source_sku', $feed_id, 'set_arr');
	switch ($ipytw_source_sku) {
		case 'vendor_code':
			$product_sku = (string)$data['offer_xml_object']->vendorCode;
			if (ipytw_is_uniq_sku($product_sku) === false) {$product->set_sku($product_sku);}				
		break;
		case 'shop_sku':
			$el_name = 'shop-sku';
			$product_sku = (string)$data['offer_xml_object']->$el_name;
			if (ipytw_is_uniq_sku($product_sku) === false) {$product->set_sku($product_sku);}
		break;
		case 'sku':
			$product_sku = (string)$data['offer_xml_object']->sku;
			if (ipytw_is_uniq_sku($product_sku) === false) {$product->set_sku($product_sku);}		
		break;
		case 'article':
			$product_sku = (string)$data['offer_xml_object']->article;
			if (ipytw_is_uniq_sku($product_sku) === false) {$product->set_sku($product_sku);}		
		break;
		case 'product_id':
			$product_sku = (string)$data['feed_product_id'];
			if (ipytw_is_uniq_sku($product_sku) === false) {$product->set_sku($product_sku);}
		break;
		default:
			do_action('ipytw_switch_source_sku_action', $product, $ipytw_source_sku, $data['offer_xml_object'], $feed_id); 
	}

	if (property_exists($data['offer_xml_object'], 'vendor')) {
		$vendor_value = (string)$data['offer_xml_object']->vendor; 
		$attribute_name = "Производитель";
		$attribute_value = $vendor_value;
		$attribute_id = (int)ipytw_create_product_attribute($attribute_name); // есть ли глобальный атрибут (при необходимости функция его создаст)
		$attribute_slug = wc_attribute_taxonomy_slug($attribute_name); // слаг этого атрибута
		$attribute_pa_taxonomy = wc_attribute_taxonomy_name_by_id($attribute_id); // 'pa_'.$attribute_slug;

		$date_of_new_attribute_arr = ipytw_create_attribute_term($attribute_value, $attribute_id); // есть ли такое значение у атрибута
		$attribute_term_id = $date_of_new_attribute_arr['id']; // id значения атрибута
		$attribute_term_slug = $date_of_new_attribute_arr['slug']; // слаг значения атрибута
		
		// $attributes = $product->get_attributes(); // получили все атрибуты товара	
		$attributes = (array)$product->get_attributes();

		// прописываем товару новый атрибут 
		$attribute = new WC_Product_Attribute();
		$attribute->set_id(sizeof($attributes) + 1);
		$attribute->set_name($attribute_pa_taxonomy);
		$attribute->set_options(array($attribute_term_id));
		$attribute->set_position(sizeof($attributes) + 1);
		$attribute->set_visible(true);
		$attribute->set_variation(false);
		$attributes[] = $attribute; // Добавляем к массиву атрибутов - наш атрибут

		$product->set_attributes($attributes); 
		$site_product_id = $product->save();
		unset($attribute);
		$product = wc_get_product($site_product_id);
	}

	if (property_exists($data['offer_xml_object'], 'param')) {
	$offer_xml_params_object = $data['offer_xml_object']->param;
	for ($i = 0; $i < count($offer_xml_params_object); $i++) {
		$params_attributes_object = $offer_xml_params_object[$i]->attributes();
		$attribute_name = (string)$params_attributes_object->name;
		$attribute_value = (string)$offer_xml_params_object[$i];
		$attribute_id = (int)ipytw_create_product_attribute($attribute_name); // есть ли глобальный атрибут (при необходимости функция его создаст)
		$attribute_slug = wc_attribute_taxonomy_slug($attribute_name); // слаг этого атрибута
		$attribute_pa_taxonomy = wc_attribute_taxonomy_name_by_id($attribute_id); // 'pa_'.$attribute_slug;

		$date_of_new_attribute_arr = ipytw_create_attribute_term($attribute_value, $attribute_id); // есть ли такое значение у атрибута
		$attribute_term_id = $date_of_new_attribute_arr['id']; // id значения атрибута
		$attribute_term_slug = $date_of_new_attribute_arr['slug']; // слаг значения атрибута
		
		// $attributes = $product->get_attributes(); // получили все атрибуты товара	
		$attributes = (array)$product->get_attributes();

		// прописываем товару новый атрибут 
		$attribute = new WC_Product_Attribute();
		$attribute->set_id(sizeof($attributes) + 1);
		$attribute->set_name($attribute_pa_taxonomy);
		$attribute->set_options(array($attribute_term_id));
		$attribute->set_position(sizeof($attributes) + 1);
		$attribute->set_visible(true);
		$attribute->set_variation(false);
		$attributes[] = $attribute; // Добавляем к массиву атрибутов - наш атрибут

		$product->set_attributes($attributes); 
	}
	}

	$ipytw_post_status = ipytw_optionGET('ipytw_post_status', $feed_id, 'set_arr');	
	$product->set_status($ipytw_post_status);
	$product = apply_filters('ipytw_product_add_before_save_filter', $product, array('data' => $data, 'flag' => $flag), $feed_id);
	$product_id = $product->save();
	
	$unixtime = current_time('Y-m-d H:i');
	update_post_meta($site_product_id, '_ipytw_date_last_import', $unixtime);
	$imported_ids = array();
	$imported_ids['feed_product_id'] = $data['feed_product_id'];
	$imported_ids['site_product_id'] = $site_product_id;
	$imported_ids['feed_product_cat'] = $feed_product_cat;
	$imported_ids['site_product_cat'] = $site_product_cat;
	$imported_ids['product_price'] = $product_price;
	unset($product);
	return $imported_ids;
} // end function ipytw_product_add($data, $feed_id) {
?>