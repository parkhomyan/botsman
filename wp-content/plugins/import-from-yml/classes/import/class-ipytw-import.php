<?php if (!defined('ABSPATH')) {exit;}
/**
* Import of products
*
* @link			https://icopydoc.ru/
* @since		1.0.0
*/

class IPYTW_Import {
	private $feed_id;
	private $last_element;
	private $timeout;

	function __construct($args_arr) {		
		if (isset($args_arr['feed_id'])) {
			$this->feed_id = $args_arr['feed_id'];
		}
		if (isset($args_arr['last_element'])) {
			$this->last_element = $args_arr['last_element'];
		};
		if (isset($args_arr['timeout'])) {
			$this->timeout = $args_arr['timeout'];
		}
	}

	public function stop() {
		$status_sborki = -1;
		ipytw_optionUPD('ipytw_status_sborki', $status_sborki, $this->get_feed_id());
		ipytw_optionUPD('ipytw_last_element', '-1', $this->get_feed_id());
		ipytw_optionUPD('ipytw_count_elements', '-1', $this->get_feed_id());
		wp_clear_scheduled_hook('ipytw_cron_sborki', array($this->get_feed_id())); // останавливаем крон сборки
		do_action('ipytw_after_construct', 'full'); // сборка закончена
	}

	public function import_cat() {
		$unixtime = current_time('Y-m-d H:i'); // время в unix формате 
		ipytw_optionUPD('ipytw_date_sborki', $unixtime, $this->get_feed_id(), 'yes', 'set_arr');

		$ipytw_when_import_category = ipytw_optionGET('ipytw_when_import_category', $this->get_feed_id(), 'set_arr'); 
 		if ($ipytw_when_import_category === 'always' || $ipytw_when_import_category === 'once') { 
			$xml_object = $this->get_feed_our_site();
			$xml_categories_object = $xml_object->shop->categories;
			$feed_categories_arr = array(); // данные по всем категориям в фиде
			$feed_exists_cat_id_arr = array(); // id всех существующих категорий в фиде. Нужен для проверки.
			for ($i = 0; $i < count($xml_categories_object->children()); $i++) {
				$category_name = $xml_categories_object->category[$i];
				$xml_category_attr_object = $xml_categories_object->category[$i]->attributes();
			
				$attr_id = $xml_category_attr_object->id;
				$attr_parentId = $xml_category_attr_object->parentId; // если нет атрибута parentId вернёт null
				if ($attr_parentId === null) {$attr_parentId = 0;}
				$feed_categories_arr[] = array(
					'id' => (int)$attr_id,
					'parent_id' => (int)$attr_parentId,
					'name' => (string)$category_name
				);
				$feed_exists_cat_id_arr[] = (int)$attr_id;
			}
		
			$i = 0; 
			$find_cat_arr = array();
			while (0 < count($feed_categories_arr)) {
				new IPYTW_Error_Log('$feed_categories_arr['.$i.'][id] = '.$feed_categories_arr[$i]['id']);
				if ($feed_categories_arr[$i]['parent_id'] == 0) { // это родительская категория
					$category_id = $feed_categories_arr[$i]['id'];
					$attr_parentId = $feed_categories_arr[$i]['parent_id'];
					$category_name = $feed_categories_arr[$i]['name'];	
					$category = term_exists($category_name, 'product_cat');
					if ($category === null) { // таксономии нет. Нужно создать
						new IPYTW_Error_Log('Категории нет. Нужно создать. $category_name = '.$category_name.', $attr_parentId = '.$attr_parentId.'; Файл: offer.php; Строка: '.__LINE__);
						$data_arr = array();
						$data_arr['parent'] = $attr_parentId;
						$insert_data = wp_insert_term($category_name, 'product_cat', $data_arr);
						// update_term_meta($term_id, 'yml-importer:id', $id);
		
						if (!is_wp_error($insert_data)) {
							$find_cat_arr[$feed_categories_arr[$i]['id']] = $insert_data['term_id']; // добавим её id в массив найденных 
						}
					} else { // категория есть. Нужно обновить
						new IPYTW_Error_Log('категория есть. Нужно обновить. $category_id = '.$category_id.'; Файл: offer.php; Строка: '.__LINE__);
						$category_id = $category['term_id'];
						$insert_data = wp_update_term($category_id, 'product_cat', array(
							'name' => $category_name,
							'parent' => $attr_parentId
						));	
						
						if (!is_wp_error($insert_data)) {
							$find_cat_arr[$feed_categories_arr[$i]['id']] = $insert_data['term_id']; // добавим её id в массив найденных 
						}
					}
					array_splice($feed_categories_arr, $i, 1); // сократим массив
					$i = 0;
				} else { // это дочерняя категория
					// проверим от глюков в фиде
					if (!in_array($feed_categories_arr[$i]['parent_id'], $feed_exists_cat_id_arr)) {
						new IPYTW_Error_Log('NOTICE: В фиде есть ошибка! Родительской категории с id='.$feed_categories_arr[$i]['parent_id'].'. Не существует! Пропустим данную категорию; Файл: offer.php; Строка: '.__LINE__);	
						array_splice($feed_categories_arr, $i, 1);
						$i = 0; continue;
					}
		
					if (array_key_exists($feed_categories_arr[$i]['parent_id'], $find_cat_arr)) {
						// категория-родитель уже просинхронена. Можно добавить категорию.
						$category_id = $feed_categories_arr[$i]['id'];
						$attr_parentId = $find_cat_arr[$feed_categories_arr[$i]['parent_id']]; // важное различие с 1-м вариантом!
						$category_name = $feed_categories_arr[$i]['name'];	
						$category = term_exists($category_name, 'product_cat');
						if ($category === null) { // таксономии нет. Нужно создать
							$data_arr = array();
							$data_arr['parent'] = $attr_parentId;
							$insert_data = wp_insert_term($category_name, 'product_cat', $data_arr);
							// update_term_meta($term_id, 'yml-importer:id', $id);
		
							if (!is_wp_error($insert_data)) {
								$find_cat_arr[$feed_categories_arr[$i]['id']] = $insert_data['term_id']; // добавим её id в массив найденных 
							}
						} else { // категория есть. Нужно обновить
							if (is_array($category)) {
								$category_id = $category['term_id'];
								$insert_data = wp_update_term($category_id, 'product_cat', array(
			/* ! */					'name' => $category_name, // иногда даёт ошибку PHP Notice:  Trying to get property 'category_id' of non-object in хз почему
									'parent' => $attr_parentId // error_log('метка 1 $attr_parentId = '.$attr_parentId.' $category_id = '.$category_id.' $category_name = '.$category_name, 0);
								));						
								if (!is_wp_error($insert_data)) {
									$find_cat_arr[$feed_categories_arr[$i]['id']] = $insert_data['term_id']; // добавим её id в массив найденных 
								}
							} else {
								new IPYTW_Error_Log('NOTICE: Логируем $category = '.$category.';Файл: offer.php; Строка: '.__LINE__);
							}
						}
						array_splice($feed_categories_arr, $i, 1); // сократим массив
						$i = 0;
					} else { // категорию-родитель ещё не синхронили. Пока пропустим.
						$i++; continue;
					}
				}
			}

			$res = ipytw_check_dir(IPYTW_PLUGIN_UPLOADS_DIR_PATH);
			if ($res === false) {
				error_log('ERROR: ipytw_feed_header: Создать папку IPYTW_PLUGIN_UPLOADS_DIR_PATH ='.IPYTW_PLUGIN_UPLOADS_DIR_PATH.' не вышло; Файл: offer.php; Строка: '.__LINE__, 0);
				return false;
			}
		
			if (is_dir(IPYTW_PLUGIN_UPLOADS_DIR_PATH)) {
				$filename = IPYTW_PLUGIN_UPLOADS_DIR_PATH.'/feed-importetd-cat-ids-'.$this->get_feed_id().'.tmp';
				$fp = fopen($filename, "w");
				$ids_in_yml = serialize($find_cat_arr);
				fwrite($fp, $ids_in_yml);
				fclose($fp);
				if ($ipytw_when_import_category === 'once') { 
					ipytw_optionUPD('ipytw_when_import_category', 'disabled', $this->get_feed_id(), 'yes', 'set_arr');
				}
			} else {
				error_log('ERROR: ipytw_feed_header: Нет папки import-from-yml! IPYTW_PLUGIN_UPLOADS_DIR_PATH ='.IPYTW_PLUGIN_UPLOADS_DIR_PATH.'; Файл: offer.php; Строка: '.__LINE__, 0);
				return false;
			}
		} // end if ($ipytw_when_import_category === 'always' || $ipytw_when_import_category === 'once')
		return true;
	} // end public function import_cat()

	public function set_count_elements() {
		// true - элементы найдены и их больше 0
		// false - в остальных случаях
		
		$xml_object = $this->get_feed_our_site();
		$xml_offers_object = $xml_object->shop->offers;	
		$ipytw_count_elements = count($xml_offers_object->children());

		if ($ipytw_count_elements > 0) {
			ipytw_optionUPD('ipytw_count_elements', $ipytw_count_elements, $this->get_feed_id(), 'yes', 'set_arr');
			return true;
		} else {
			return false;
		}
	}

	public function get_feed_our_site() {
		$filename = IPYTW_PLUGIN_UPLOADS_DIR_PATH.'/'.$this->get_feed_id().'.xml';
		$xml_string = file_get_contents($filename);
		$xml_object = new SimpleXMLElement($xml_string);
		return $xml_object;
	}

	public function get_feed_id() {
		return $this->feed_id; 
	}

	public function get_last_element() {
		return $this->last_element; 
	}

	public function get_timeout() {
		return $this->timeout; 
	}
}
?>