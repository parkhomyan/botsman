<?php // https://2web-master.ru/wp_list_table-%E2%80%93-poshagovoe-rukovodstvo.html https://wp-kama.ru/function/wp_list_table
class EOPN_WP_List_Table extends WP_List_Table {
	/*	Метод get_columns() необходим для маркировки столбцов внизу и вверху таблицы. 
	*	Ключи в массиве должны быть теми же, что и в массиве данных, 
	*	иначе соответствующие столбцы не будут отображены.
	*/
	function get_columns() {
		$columns = array(
			'cb'					=> '<input type="checkbox" />', // флажок сортировки. см get_bulk_actions и column_cb
			'ID'					=> __('Feed ID', 'ipytw'),
			'ipytw_feed_assignment' => __('Feed assignment', 'ipytw'),
			'ipytw_url_yml_file'	=> __('YML File', 'ipytw'),
			'ipytw_run_cron'		=> __('Start automatic import', 'ipytw'),
			'ipytw_step_import'		=> __('Step of import', 'ipytw'),
		);
		return $columns;
	}
	/*	
	*	Метод вытаскивает из БД данные, которые будут лежать в таблице
	*	$this->table_data();
	*/
	private function table_data() {
		$result_arr = array();
		$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
		for ($i = 1; $i < count($ipytw_registered_feeds_arr); $i++) { // с единицы, т.к инфа по конкретным фидам там
			$feedId = $ipytw_registered_feeds_arr[$i]['id'];
			$ipytw_status_cron = ipytw_optionGET('ipytw_status_cron', $feedId, 'set_arr');
			switch($ipytw_status_cron) {
				case 'off': $text = __("Don't start", "ipytw"); break;
				case 'five_min':  $text = __('Every five minutes', 'ipytw'); break;
				case 'hourly':  $text = __('Hourly', 'ipytw'); break;
				case 'six_hours':  $text = __('Every six hours', 'ipytw'); break;
				case 'twicedaily':  $text = __('Twice a day', 'ipytw'); break;
				case 'daily':  $text = __('Daily', 'ipytw'); break;
				case 'every_two_days':  $text = __('Every two days', 'ipytw'); break;
				default: $text = __("Don't start", "ipytw"); 
			}
			$ipytw_url_yml_file = ipytw_optionGET('ipytw_url_yml_file', $feedId, 'set_arr'); 
			if ($ipytw_url_yml_file == '') {
				$url_yml_file = __('No feed address set', 'ipytw');
			} else {
				$url_yml_file = sprintf('<a href="%s" alt="%s">%s</a>', $ipytw_url_yml_file, $ipytw_url_yml_file, $ipytw_url_yml_file);
			}
			$ipytw_step_import = ipytw_optionGET('ipytw_step_import', $feedId, 'set_arr'); 
			$ipytw_feed_assignment = ipytw_optionGET('ipytw_feed_assignment', $feedId, 'set_arr');
			$result_arr[$i] = array(
				'ID' => $feedId,
				'ipytw_feed_assignment' => $ipytw_feed_assignment,
				'ipytw_url_yml_file' => $url_yml_file,
				'ipytw_run_cron' => $text,
				'ipytw_step_import' => $ipytw_step_import.' '. __('sec', 'ipytw')
			);
		}
		return $result_arr;
	}
	/*
	*	prepare_items определяет два массива, управляющие работой таблицы:
	*	$hidden определяет скрытые столбцы https://2web-master.ru/wp_list_table-%E2%80%93-poshagovoe-rukovodstvo.html#screen-options
	*	$sortable определяет, может ли таблица быть отсортирована по этому столбцу.
	*
	*/
	function prepare_items() {
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns(); // вызов сортировки
		$this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = $this->table_data(); // Получаем данные для формирования таблицы
	}
	/*
	* 	Данные таблицы.
	*	Наконец, метод назначает данные из примера на переменную представления данных класса — items.
	*	Прежде чем отобразить каждый столбец, WordPress ищет методы типа column_{key_name}, например, function column_ipytw_url_yml_file. 
	*	Такой метод должен быть указан для каждого столбца. Но чтобы не создавать эти методы для всех столбцов в отдельности, 
	*	можно использовать column_default. Эта функция обработает все столбцы, для которых не определён специальный метод:
	*/ 
	function column_default($item, $column_name) {
		switch( $column_name ) {
			case 'ID':
			case 'ipytw_feed_assignment':
			case 'ipytw_url_yml_file':
			case 'ipytw_run_cron':
			case 'ipytw_step_import':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ) ; //Мы отображаем целый массив во избежание проблем
		}
	}
	/*
	* 	Функция сортировки.
	*	Второй параметр в массиве значений $sortable_columns отвечает за порядок сортировки столбца. 
	*	Если значение true, столбец будет сортироваться в порядке возрастания, если значение false, столбец сортируется в порядке 
	*	убывания, или не упорядочивается. Это необходимо для маленького треугольника около названия столбца, который указывает порядок
	*	сортировки, чтобы строки отображались в правильном направлении
	*/
	function get_sortable_columns() {
		$sortable_columns = array(
			'ipytw_url_yml_file'	=> array('ipytw_url_yml_file', false),
			// 'ipytw_run_cron'		=> array('ipytw_run_cron', false)
		);
		return $sortable_columns;
	}
	/*
	* 	Действия.
	*	Эти действия появятся, если пользователь проведет курсор мыши над таблицей
	*	column_{key_name} - в данном случае для колонки ipytw_url_yml_file - function column_ipytw_url_yml_file
	*/
	function column_ipytw_url_yml_file($item) {
	/* ! */
		if ($item['ID'] === '-1') {
			$actions = array(
				'edit'		=> sprintf('<a href="?page=%s&action=%s&numFeed=%s">Edit</a>', $_REQUEST['page'], 'edit', $item['ID'])
			);
		} else {
			$actions = array(
				'edit'		=> sprintf('<a href="?page=%s&action=%s&numFeed=%s">Edit</a>', $_REQUEST['page'], 'edit', $item['ID']),
				'delete'	=> sprintf('<a href="?page=%s&action=%s&numFeed=%s">Delete</a>', $_REQUEST['page'], 'delete', $item['ID']),
			);
		}
		return sprintf('%1$s %2$s', $item['ipytw_url_yml_file'], $this->row_actions($actions) );
	}
	/*
	* 	Массовые действия.
	*	Bulk action осуществляются посредством переписывания метода get_bulk_actions() и возврата связанного массива
	*	Этот код просто помещает выпадающее меню и кнопку «применить» вверху и внизу таблицы
	*	ВАЖНО! Чтобы работало нужно оборачивать вызов класса в form:
	*	<form id="events-filter" method="get"> 
	*	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" /> 
	*	<?php $wp_list_table->display(); ?> 
	*	</form> 
	*/
	function get_bulk_actions() {
		$actions = array(
			'delete'	=> __('Delete', 'ipytw')
		);
		return $actions;
	}
	// Флажки для строк должны быть определены отдельно. Как упоминалось выше, есть метод column_{column} для отображения столбца. cb-столбец – особый случай:
	function column_cb($item) {
	/* ! */
		if ($item['ID'] === '-1') {
			return sprintf(
				'<input type="checkbox" name="checkbox_yml_file[]" value="%s" disabled />', $item['ID']
			);
		} else {
			return sprintf(
				'<input type="checkbox" name="checkbox_yml_file[]" value="%s" />', $item['ID']
			);
		}
	}
	/*
	* Нет элементов.
	* Если в списке нет никаких элементов, отображается стандартное сообщение «No items found.». Если вы хотите изменить это сообщение, вы можете переписать метод no_items():
	*/
	function no_items() {
		_e('YML feeds not found', 'ipytw');
	}
}
?>