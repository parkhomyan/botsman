<?php if (!defined('ABSPATH')) {exit;}
/**
* Plugin Updates
*
* @link			https://icopydoc.ru/
* @since		1.6.0
*/

class IPYTW_Data_Arr {
	private $data_arr = [
		array('ipytw_status_cron', 'off', 'private'),
		array('ipytw_date_sborki', '0000000001', 'private'), // дата начала сборки
		array('ipytw_date_sborki_end', '0000000001', 'private'), // дата завершения сборки
		array('ipytw_date_save_set', '0000000001', 'private'), // дата сохранения настроек плагина
		array('ipytw_count_elements', -1, 'public'),
		array('ipytw_run_cron', 'off', 'public'),
		array('ipytw_feed_assignment', '', 'public'),
		array('ipytw_url_yml_file', '', 'public'),
		array('ipytw_dir_copy_feed_from_source', '', 'public'),
		array('ipytw_when_import_category', 'always', 'public'),
		array('ipytw_product_was_sync', 'price_only', 'public'),
		array('ipytw_post_status', 'publish', 'public'),
		array('ipytw_step_import', 25, 'public'),
		array('ipytw_description_into', 'full', 'public'),
		array('ipytw_source_sku', 'disabled', 'public'),
		array('ipytw_if_isset_sku', 'update', 'public'),
		array('ipytw_missing_product', 'disabled', 'public'),
		array('ipytw_fsize_limit', 5, 'public'),
		array('ipytw_count_offers', 0, 'public')
	];

	public function __construct($data_arr = array()) {
		if (!empty($data_arr)) {
			$this->data_arr = $data_arr;
		}		
		$this->data_arr = apply_filters('ipytw_set_default_feed_settings_result_arr_filter', $this->data_arr);
	}

	public function get_data_arr() {
		return $this->data_arr;
	}

	// @return array([0] => opt_key1, [1] => opt_key2, ...)
	public function get_opts_name($whot = '') {
		if ($this->data_arr) {
			$res_arr = array();		
			for ($i = 0; $i < count($this->data_arr); $i++) {
				switch ($whot) {
					case "public":
						if ($this->data_arr[$i][2] === 'public') {
							$res_arr[] = $this->data_arr[$i][0];
						}
					break;
					case "private":
						if ($this->data_arr[$i][2] === 'private') {
							$res_arr[] = $this->data_arr[$i][0];
						}
					break;
					default:
						$res_arr[] = $this->data_arr[$i][0];
				}
			}
			return $res_arr;
		} else {
			return array();
		}
	}

	// @return array(opt_name1 => opt_val1, opt_name2 => opt_val2, ...)
	public function get_opts_name_and_def_date($whot = 'all') {
		if ($this->data_arr) {
			$res_arr = array();		
			for ($i = 0; $i < count($this->data_arr); $i++) {
				switch ($whot) {
					case "public":
						if ($this->data_arr[$i][2] === 'public') {
							$res_arr[$this->data_arr[$i][0]] = $this->data_arr[$i][1];
						}
					break;
					case "private":
						if ($this->data_arr[$i][2] === 'private') {
							$res_arr[$this->data_arr[$i][0]] = $this->data_arr[$i][1];
						}
					break;
					default:
						$res_arr[$this->data_arr[$i][0]] = $this->data_arr[$i][1];
				}
			}
			return $res_arr;
		} else {
			return array();
		}
	}

	public function get_opts_name_and_def_date_obj($whot = 'all') {		
		$source_arr = $this->get_opts_name_and_def_date($whot);

		$res_arr = array();	
		foreach($source_arr as $key => $value) {
			$res_arr[] = new IPYTW_Data_Arr_Helper($key, $value); // return unit obj
		}
		return $res_arr;
	}
}
class IPYTW_Data_Arr_Helper {	
	private $opt_name;
	private $opt_def_value;

	function __construct($name = '', $def_value = '') {
		$this->opt_name = $name;
		$this->opt_def_value = $def_value;
	}

	function get_name() {
		return $this->opt_name;
	}

	function get_value() {
		return $this->opt_def_value;
	}
}
?>