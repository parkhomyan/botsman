<?php if (!defined('ABSPATH')) {exit;} // Защита от прямого вызова скрипта
function ipytw_export_page() {
 // массовое удаление фидов по чекбоксу checkbox_yml_file
 if (isset($_GET['ipytw_form_id']) && ($_GET['ipytw_form_id'] === 'ipytw_wp_list_table')) {
	if (is_array($_GET['checkbox_yml_file']) && !empty($_GET['checkbox_yml_file'])) {
		if ($_GET['action'] === 'delete' || $_GET['action2'] === 'delete') {
			$checkbox_yml_file_arr = $_GET['checkbox_yml_file'];
			$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
			for ($i = 0; $i < count($checkbox_yml_file_arr); $i++) {
				$feed_id = $checkbox_yml_file_arr[$i];
				unset($ipytw_settings_arr[$feed_id]);
				wp_clear_scheduled_hook('ipytw_cron_period', array($feed_id)); // отключаем крон
				wp_clear_scheduled_hook('ipytw_cron_sborki', array($feed_id)); // отключаем крон
				$upload_dir = (object)wp_get_upload_dir();
				$name_dir = $upload_dir->basedir."/import-from-yml";
				$filename = $name_dir.'/feed-imported-offers-'.$i.'.tmp'; $res = unlink($filename);
				$filename = $name_dir.'/feed-importetd-cat-ids-'.$i.'.tmp'; $res = unlink($filename);
				ipytw_optionDEL('ipytw_status_sborki', $i);
				ipytw_optionDEL('ipytw_last_element', $i);

				$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
				for ($n = 1; $n < count($ipytw_registered_feeds_arr); $n++) { // первый элемент не проверяем, тк. там инфо по последнему id
					if ($ipytw_registered_feeds_arr[$n]['id'] === $feed_id) {
						unset($ipytw_registered_feeds_arr[$n]);
						$ipytw_registered_feeds_arr = array_values($ipytw_registered_feeds_arr);
						ipytw_optionUPD('ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
						break;
					}
				}
			}
			ipytw_optionUPD('ipytw_settings_arr', $ipytw_settings_arr);
			$feed_id = get_first_feed_id();
		}
	}
 }
 if (isset($_GET['numFeed'])) {
	if (isset($_GET['action'])) {
		$action = sanitize_text_field($_GET['action']);
		switch ($action) {
			case 'edit':
				$feed_id = sanitize_text_field($_GET['numFeed']);
				break;
			case 'delete':
				$feed_id = sanitize_text_field($_GET['numFeed']);
				$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
				unset($ipytw_settings_arr[$feed_id]);
				wp_clear_scheduled_hook('ipytw_cron_period', array($feed_id)); // отключаем крон
				wp_clear_scheduled_hook('ipytw_cron_sborki', array($feed_id)); // отключаем крон
				$upload_dir = (object)wp_get_upload_dir();
				$name_dir = $upload_dir->basedir."/import-from-yml";
				$filename = $name_dir.'/feed-imported-offers-'.$feed_id.'.tmp'; if (file_exists($filename)) {unlink($filename);}
				$filename = $name_dir.'/feed-importetd-cat-ids-'.$feed_id.'.tmp'; if (file_exists($filename)) {unlink($filename);}
				ipytw_optionUPD('ipytw_settings_arr', $ipytw_settings_arr);
				ipytw_optionDEL('ipytw_status_sborki', $feed_id);
				ipytw_optionDEL('ipytw_last_element', $feed_id);

				$ipytw_registered_feeds_arr = ipytw_optionGET('ipytw_registered_feeds_arr');
				for ($n = 1; $n < count($ipytw_registered_feeds_arr); $n++) { // первый элемент не проверяем, тк. там инфо по последнему id
					if ($ipytw_registered_feeds_arr[$n]['id'] === $feed_id) {
						unset($ipytw_registered_feeds_arr[$n]);
						$ipytw_registered_feeds_arr = array_values($ipytw_registered_feeds_arr); 
						ipytw_optionUPD('ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
						break;
					}
				}

				$feed_id = get_first_feed_id();
				break;
			default:
				$feed_id = get_first_feed_id();
		}
	} else {$feed_id = get_first_feed_id();}
 } else {$feed_id = get_first_feed_id();}
 if (isset($_REQUEST['ipytw_submit_add_new_feed'])) { // если создаём новый фид
	if (!empty($_POST) && check_admin_referer('ipytw_nonce_action_add_new_feed', 'ipytw_nonce_field_add_new_feed')) {
		$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');

		/*
		$ipytw_settings_arr_keys_arr = array_keys($ipytw_settings_arr); // отсортируем массив по ключам
		$last_key = count($ipytw_settings_arr_keys_arr)-1; // получим ключ последнего элемента.
		$feed_id = (int)$ipytw_settings_arr_keys_arr[$last_key]; // id создаваемого фида
		*/

		if (is_multisite()) {
			$ipytw_registered_feeds_arr = get_blog_option(get_current_blog_id(), 'ipytw_registered_feeds_arr');
			$feed_id = $ipytw_registered_feeds_arr[0]['last_id'];
			$feed_id++;
			$ipytw_registered_feeds_arr[0]['last_id'] = (string)$feed_id;
			$ipytw_registered_feeds_arr[] = array('id' => (string)$feed_id);
			update_blog_option(get_current_blog_id(), 'ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
		} else {
			$ipytw_registered_feeds_arr = get_option('ipytw_registered_feeds_arr');
			$feed_id = $ipytw_registered_feeds_arr[0]['last_id'];
			$feed_id++;
			$ipytw_registered_feeds_arr[0]['last_id'] = (string)$feed_id;
			$ipytw_registered_feeds_arr[] = array('id' => (string)$feed_id);
			update_option('ipytw_registered_feeds_arr', $ipytw_registered_feeds_arr);
		}

		$ipytw_data_arr_obj = new IPYTW_Data_Arr();
		$opts_arr = $ipytw_data_arr_obj->get_opts_name_and_def_date('all');

		$ipytw_settings_arr[$feed_id] = $opts_arr;
		ipytw_optionUPD('ipytw_settings_arr', $ipytw_settings_arr);

		ipytw_optionADD('ipytw_status_sborki', '-1', $feed_id);
		ipytw_optionADD('ipytw_last_element', '-1', $feed_id);
		print '<div class="updated notice notice-success is-dismissible"><p>'. __('Feed added', 'ipytw').'. ID = '.$feed_id.'.</p></div>';
	}
 }
 
 $ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr'); 
 if (isset($_REQUEST['ipytw_submit_action'])) {
  if (!empty($_POST) && check_admin_referer('ipytw_nonce_action', 'ipytw_nonce_field')) {
	do_action('ipytw_prepend_submit_action', $feed_id);
	
	$feed_id = (int)sanitize_text_field($_POST['ipytw_num_feed_for_save']);
	$ipytw_settings_arr[$feed_id]['ipytw_url_yml_file'] = sanitize_text_field($_POST['ipytw_url_yml_file']);

	if (!empty($_FILES['ipytw_image_upload'])) {	
		if ($_FILES['ipytw_image_upload']['name'] !== '') {
			require_once(ABSPATH. 'wp-admin/includes/image.php');
			require_once(ABSPATH. 'wp-admin/includes/file.php');
			require_once(ABSPATH. 'wp-admin/includes/media.php');

			// Позволим WordPress перехватить загрузку. 
			// не забываем указать атрибут name поля input - 'ipytw_image_upload'
			$attachment_id = media_handle_upload('ipytw_image_upload', 0); // 0 - не прикреплятьк посту

			if (is_wp_error($attachment_id)) {
				printf('<div class="error notice notice-error is-dismissible"><p>YML %1$s %2$s! %3$s</p></div>', __('feed', 'ipytw'), __('upload error', 'ipytw'), $attachment_id->get_error_message());
			} else {
				$feed_url = wp_get_attachment_url($attachment_id);
				$ipytw_settings_arr[$feed_id]['ipytw_url_yml_file'] = $feed_url;
			}
		}
  	}

	$ipytw_settings_arr[$feed_id]['ipytw_feed_assignment'] = sanitize_text_field($_POST['ipytw_feed_assignment']);
//	$ipytw_settings_arr[$feed_id]['ipytw_url_yml_file'] = sanitize_text_field($_POST['ipytw_url_yml_file']);
	$ipytw_settings_arr[$feed_id]['ipytw_when_import_category'] = sanitize_text_field($_POST['ipytw_when_import_category']);
	$ipytw_settings_arr[$feed_id]['ipytw_product_was_sync'] = sanitize_text_field($_POST['ipytw_product_was_sync']);
	$ipytw_settings_arr[$feed_id]['ipytw_post_status'] = sanitize_text_field($_POST['ipytw_post_status']);
	
	$unixtime = current_time('timestamp', 1); // 1335808087 - временная зона GMT (Unix формат)
	$ipytw_settings_arr[$feed_id]['ipytw_date_save_set'] = $unixtime;
	$ipytw_settings_arr[$feed_id]['ipytw_step_import'] = sanitize_text_field($_POST['ipytw_step_import']);
	$ipytw_settings_arr[$feed_id]['ipytw_description_into'] = sanitize_text_field($_POST['ipytw_description_into']);
	$ipytw_settings_arr[$feed_id]['ipytw_source_sku'] = sanitize_text_field($_POST['ipytw_source_sku']);
	$ipytw_settings_arr[$feed_id]['ipytw_if_isset_sku'] = sanitize_text_field($_POST['ipytw_if_isset_sku']);	
	$ipytw_settings_arr[$feed_id]['ipytw_missing_product'] = sanitize_text_field($_POST['ipytw_missing_product']);
	$ipytw_settings_arr[$feed_id]['ipytw_fsize_limit'] = (int)sanitize_text_field($_POST['ipytw_fsize_limit']);

	$ipytw_settings_arr = apply_filters('ipytw_upd_settings_arr_filter', $ipytw_settings_arr, $feed_id);
	ipytw_optionUPD('ipytw_settings_arr', $ipytw_settings_arr);

	$arr_maybe = array("off", "five_min", "hourly", "six_hours", "twicedaily", "daily", "every_two_days");
	$ipytw_run_cron = sanitize_text_field($_POST['ipytw_run_cron']);
	if (in_array($ipytw_run_cron, $arr_maybe)) {		
		$ipytw_settings_arr[$feed_id]['ipytw_status_cron'] = $ipytw_run_cron;
		if ($ipytw_run_cron === 'off') {
			// отключаем крон
			wp_clear_scheduled_hook('ipytw_cron_period', array($feed_id));
			$ipytw_settings_arr[$feed_id]['ipytw_status_cron'] = 'off';
			
			wp_clear_scheduled_hook('ipytw_cron_sborki', array($feed_id));
			ipytw_optionUPD('ipytw_status_sborki', '-1', $feed_id);
			ipytw_optionUPD('ipytw_last_element', '-1', $feed_id);
			$ipytw_settings_arr[$feed_id]['ipytw_count_elements'] = -1;
		} else {
			ipytw_optionUPD('ipytw_status_sborki', '-1', $feed_id);
			ipytw_optionUPD('ipytw_last_element', '-1', $feed_id); // в теории тут можно регулировать "продолжить импорт" или "с нуля"
			$recurrence = $ipytw_run_cron;
			wp_clear_scheduled_hook('ipytw_cron_period', array($feed_id));
			wp_schedule_event(time(), $recurrence, 'ipytw_cron_period', array($feed_id));
			new IPYTW_Error_Log('FEED № '.$feed_id.'; ipytw_cron_period внесен в список заданий; Файл: export.php; Строка: '.__LINE__);
		}
		ipytw_optionUPD('ipytw_settings_arr', $ipytw_settings_arr);
	} else {
		new IPYTW_Error_Log('Крон '.$ipytw_run_cron.' не зарегистрирован. Файл: export.php; Строка: '.__LINE__);
	}
  }
 } 

 $ipytw_status_cron = ipytw_optionGET('ipytw_status_cron', $feed_id, 'set_arr');
 $ipytw_feed_assignment = ipytw_optionGET('ipytw_feed_assignment', $feed_id, 'set_arr');
 $ipytw_url_yml_file = ipytw_optionGET('ipytw_url_yml_file', $feed_id, 'set_arr');
 $ipytw_when_import_category = ipytw_optionGET('ipytw_when_import_category', $feed_id, 'set_arr');
 $ipytw_product_was_sync = ipytw_optionGET('ipytw_product_was_sync', $feed_id, 'set_arr');
 $ipytw_post_status = ipytw_optionGET('ipytw_post_status', $feed_id, 'set_arr');
 $ipytw_step_import = ipytw_optionGET('ipytw_step_import', $feed_id, 'set_arr');
 $ipytw_description_into = ipytw_optionGET('ipytw_description_into', $feed_id, 'set_arr');
 $ipytw_source_sku = ipytw_optionGET('ipytw_source_sku', $feed_id, 'set_arr');
 $ipytw_if_isset_sku = ipytw_optionGET('ipytw_if_isset_sku', $feed_id, 'set_arr');
 $ipytw_missing_product = ipytw_optionGET('ipytw_missing_product', $feed_id, 'set_arr');
 $ipytw_fsize_limit = ipytw_optionGET('ipytw_fsize_limit', $feed_id, 'set_arr');
?>
<div class="wrap">
 <h1><?php _e('Import products from YML to WooCommerce', 'ipytw'); ?></h1>
 <?php $woo_version = get_woo_version_number();
	if ($woo_version <= 3.0 ) {
		print '<div class="notice notice-error is-dismissible"><p>'. __('For the plugin to function correctly, you need a version of WooCommerce 3.0 and higher! You have version ', 'ipytw'). $woo_version . __(' installed. Please, update WooCommerce', 'ipytw'). '! <a href="https://icopydoc.ru/minimalnye-trebovaniya-dlya-raboty-import-from-yml/?utm_source=import-from-yml&utm_medium=organic&utm_campaign=in-plugin-import-from-yml&utm_content=settings&utm_term=update-woocommerce">'. __('Learn More', 'ipytw'). '</a>.</p></div>';
	}
	if (defined('DISABLE_WP_CRON')) {
	 	if (DISABLE_WP_CRON == true) {
			print '<div class="notice notice-error is-dismissible"><p>'. __('Most likely, the plugin does not work correctly because you turned off the CRON with the help of the ', 'ipytw'). 'DISABLE_WP_CRON.</p></div>';
	 	}
	}
 ?> 
 <?php do_action('ipytw_before_poststuff', $feed_id); ?>
 <div id="poststuff"><?php $ipytwListTable = new EOPN_WP_List_Table(); ?>
  <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
  	<?php wp_nonce_field('ipytw_nonce_action_add_new_feed', 'ipytw_nonce_field_add_new_feed'); ?><input class="button" type="submit" name="ipytw_submit_add_new_feed" value="<?php _e('Add New Feed', 'ipytw'); ?>" />
  </form>
  <form method="get">
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
	<input type="hidden" name="ipytw_form_id" value="ipytw_wp_list_table" />
	<?php $ipytwListTable->prepare_items(); $ipytwListTable->display(); ?>
  </form>
  <div id="post-body" class="columns-2">
  <div id="postbox-container-1" class="postbox-container"><div class="meta-box-sortables">
  	<?php do_action('ipytw_prepend_container_1', $feed_id); ?>
	<?php do_action('ipytw_before_support_project'); ?>
	<div class="postbox">
	 <h2 class="hndle"><?php _e('Please support the project', 'ipytw'); ?>!</h2>
	 <div class="inside">	  
		<p><?php _e('Thank you for using the plugin', 'ipytw'); ?> <strong>Import from YML</strong></p>
		<p><?php _e('Please help make the plugin better', 'ipytw'); ?> <a href="//forms.gle/6nNnJfffQytsPEVb8" target="_blank" ><?php _e('answering 6 questions', 'ipytw'); ?>!</a></p>
		<p><?php _e('If this plugin useful to you, please support the project one way', 'ipytw'); ?>:</p>
		<ul class="ipytw_ul">
			<li><a href="//wordpress.org/support/plugin/import-from-yml/reviews/" target="_blank"><?php _e('Leave a comment on the plugin page', 'ipytw'); ?></a>.</li>
			<li><?php _e('Support the project financially', 'ipytw'); ?>. <a href="//sobe.ru/na/yml_for_yandex_market" target="_blank"> <?php _e('Donate now', 'ipytw'); ?></a>.</li>
			<li><?php _e('Noticed a bug or have an idea how to improve the quality of the plugin', 'ipytw'); ?>? <a href="mailto:support@icopydoc.ru"><?php _e('Let me know', 'ipytw'); ?></a>.</li>
		</ul>
		<p><?php _e('The author of the plugin Maxim Glazunov', 'ipytw'); ?>.</p>
		<p><span style="color: red;"><?php _e('Accept orders for individual revision of the plugin', 'ipytw'); ?></span>:<br /><a href="mailto:support@icopydoc.ru"><?php _e('Leave a request', 'ipytw'); ?></a>.</p>
	  </div>
	</div>		
	<?php do_action('ipytw_between_container_1', $feed_id); ?>
	<div class="postbox">
	  <h2 class="hndle"><?php _e('Send data about the work of the plugin', 'ipytw'); ?></h2>
	  <div class="inside">	  
		<p><?php _e('Sending statistics you help make the plugin even better', 'ipytw'); ?>! <?php _e('The following data will be transferred', 'ipytw'); ?>:</p>
		<ul class="ipytw_ul">
			<li><?php _e('Site URL', 'ipytw'); ?></li>
			<li><?php _e('URL YML-feed', 'ipytw'); ?></li>
			<li><?php _e('Is the multisite mode enabled', 'ipytw'); ?>?</li>
		</ul>
		<p><?php _e('Did the plugin help you import products into WooCommerce', 'ipytw'); ?>?</p>
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
		 <p>
			<input type="radio" name="ipytw_its_ok" value="yes"><?php _e('Yes', 'ipytw'); ?><br />
			<input type="radio" name="ipytw_its_ok" value="no"><?php _e('No', 'ipytw'); ?><br />
		 </p>
		 <p><?php _e("If you don't mind to be contacted in case of problems, please enter your email address", "ipytw"); ?>. <span style="font-weight: 700;"><?php _e('And if you want a response, be sure to include your email address', 'ipytw'); ?></span>.</p>
		 <p><input type="email" name="ipytw_email"></p>
		 <p><?php _e("Your message", "ipytw"); ?>:</p>
		 <p><textarea rows="6" cols="32" name="ipytw_message" placeholder="<?php _e('Enter your text to send me a message (You can write me in Russian or English). I check my email several times a day', 'ipytw'); ?>"></textarea></p>
		 <?php wp_nonce_field('ipytw_nonce_action_send_stat', 'ipytw_nonce_field_send_stat'); ?><input class="button-primary" type="submit" name="ipytw_submit_send_stat" value="<?php _e('Send data', 'ipytw'); ?>" />
		</form>
	  </div>
	 </div>
	<?php do_action('ipytw_append_container_1', $feed_id); ?>
  </div></div>

  <div id="postbox-container-2" class="postbox-container"><div class="meta-box-sortables">
  	<?php do_action('ipytw_prepend_container_2', $feed_id); ?>
    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
	 <?php do_action('ipytw_prepend_form_container_2', $feed_id); ?>
	 <input type="hidden" name="ipytw_num_feed_for_save" value="<?php echo $feed_id; ?>">
	 <div class="postbox">
	  <h2 class="hndle"><?php _e('Main parameters', 'ipytw'); ?> <?php _e('of feed', 'ipytw'); ?> <?php echo $feed_id; ?></h2>
	   <div class="inside">
		<p><a href="https://icopydoc.ru/kak-importirovat-tovary-iz-yml-v-woocommerce-instruktsiya/?utm_source=import-from-yml&utm_medium=organic&utm_campaign=in-plugin-import-from-yml&utm_content=settings&utm_term=main-instruction" target="_blank"><?php _e('Plugin instruction', 'ipytw'); ?></a></p>
		<table class="form-table"><tbody>
		 <tr>
			<th scope="row"><label for="ipytw_run_cron"><?php _e('Start automatic import', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<select name="ipytw_run_cron" id="ipytw_run_cron">
					<option value="off" <?php selected($ipytw_status_cron, 'off' ); ?>><?php _e("Don't start", "ipytw"); ?></option>
					<?php $ipytw_enable_five_min = ipytw_optionGET('ipytw_enable_five_min'); if ($ipytw_enable_five_min === 'on') : ?>
					<option value="five_min" <?php selected($ipytw_status_cron, 'five_min');?> ><?php _e('Every five minutes', 'ipytw'); ?></option>
					<?php endif; ?>
					<option value="hourly" <?php selected($ipytw_status_cron, 'hourly');?> ><?php _e('Hourly', 'ipytw'); ?></option>
					<option value="six_hours" <?php selected($ipytw_status_cron, 'six_hours'); ?> ><?php _e('Every six hours', 'ipytw'); ?></option>	
					<option value="twicedaily" <?php selected($ipytw_status_cron, 'twicedaily');?> ><?php _e('Twice a day', 'ipytw'); ?></option>
					<option value="daily" <?php selected($ipytw_status_cron, 'daily');?> ><?php _e('Daily', 'ipytw'); ?></option>
					<option value="every_two_days" <?php selected($ipytw_status_cron, 'every_two_days');?> ><?php _e('Every two days', 'ipytw'); ?></option>
				</select><br />
				<span class="description"><?php _e('Products import interval from feed', 'ipytw'); ?></span>
			</td>
		 </tr>
		 <tr>
			<th scope="row"><label for="ipytw_feed_assignment"><?php _e('Feed assignment', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<input type="text" maxlength="20" name="ipytw_feed_assignment" id="ipytw_feed_assignment" value="<?php echo $ipytw_feed_assignment; ?>" placeholder="<?php _e('For Yandex Market', 'ipytw');?>" /><br />
				<span class="description"><?php _e("Doesn't affect imports. Inner note for your convenience", "ipytw"); ?>.</span>
			</td>
		 </tr>
		 <tr>
			<th scope="row"><label for="ipytw_url_yml_file"><?php _e('YML File', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<input type="text" name="ipytw_url_yml_file" id="ipytw_url_yml_file" value="<?php echo $ipytw_url_yml_file; ?>" placeholder="https://site.ru/feed.yml" /><br />
				<?php _e('Specify URL to the YML feed in the field above or upload the file from your computer using the button below', 'ipytw'); ?><br />
				<input type="file" name="ipytw_image_upload" id="ipytw_image_upload" multiple="false" accept=".xml,.yml"/>
			</td>
		 </tr>
		 <tr class="ipytw_tr">
			<th scope="row"><label for="ipytw_when_import_category"><?php _e('When to import categories', 'ipytw'); ?>?</label></th>
			<td class="overalldesc">
				<select name="ipytw_when_import_category" id="ipytw_when_import_category">			
				<option value="always" <?php selected($ipytw_when_import_category, 'always'); ?>><?php _e('Always', 'ipytw'); ?></option>
				<option value="once" <?php selected($ipytw_when_import_category, 'once'); ?>><?php _e('Import once', 'ipytw'); ?></option>
				<option value="disabled" <?php selected($ipytw_when_import_category, 'disabled'); ?>><?php _e('Never', 'ipytw'); ?></option>
				<?php do_action('ipytw_when_import_category_option', $feed_id, $ipytw_when_import_category); ?>
				</select>
			</td>
		 </tr>
		 <tr>
			<th scope="row"><label for="ipytw_product_was_sync"><?php _e('If the product was previously synced', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<select name="ipytw_product_was_sync" id="ipytw_product_was_sync">
				<option value="whole" <?php selected($ipytw_product_was_sync, 'whole'); ?>><?php _e('Update whole', 'ipytw'); ?></option>	
				<option value="price_only" <?php selected($ipytw_product_was_sync, 'price_only'); ?>><?php _e('Update price only', 'ipytw'); ?></option>
				<option value="price_and_stock" <?php selected($ipytw_product_was_sync, 'price_and_stock'); ?>><?php _e('Update price and stock only', 'ipytw'); ?></option>
				<option value="dont_update" <?php selected($ipytw_product_was_sync, 'dont_update'); ?>><?php _e("Don't update this product", "ipytw"); ?></option>				
				<?php do_action('ipytw_product_was_sync_option', $feed_id, $ipytw_product_was_sync); ?>
				</select>
			</td>
		 </tr>
		 <tr>
			<th scope="row"><label for="ipytw_post_status"><?php _e('Set post status after import products', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<select name="ipytw_post_status" id="ipytw_post_status">
				<option value="publish" <?php selected($ipytw_post_status, 'publish'); ?>><?php _e('Publish', 'ipytw'); ?></option>
				<option value="draft" <?php selected($ipytw_post_status, 'draft'); ?>><?php _e('Draft', 'ipytw'); ?></option>
				<option value="pending" <?php selected($ipytw_post_status, 'pending'); ?>><?php _e('Pending', 'ipytw'); ?></option>
				</select><br />
				<span class="description"><?php _e('Default', 'ipytw'); ?>: <?php _e('Publish', 'ipytw'); ?></span>
			</td>
		 </tr>
		 <tr class="ipytw_tr">
			<th scope="row"><label for="ipytw_step_import"><?php _e('Step of import', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<select name="ipytw_step_import" id="ipytw_step_import">
				<option value="10" <?php selected($ipytw_step_import, 10); ?>>10 <?php _e('sec', 'ipytw'); ?></option>
				<option value="15" <?php selected($ipytw_step_import, 15); ?>>15 <?php _e('sec', 'ipytw'); ?></option>
				<option value="20" <?php selected($ipytw_step_import, 20); ?>>20 <?php _e('sec', 'ipytw'); ?></option>
				<option value="25" <?php selected($ipytw_step_import, 25); ?>>25 <?php _e('sec', 'ipytw'); ?></option>
				<option value="30" <?php selected($ipytw_step_import, 30); ?>>30 <?php _e('sec', 'ipytw'); ?></option>
				<?php do_action('ipytw_step_import_option', $feed_id, $ipytw_step_import); ?>
				</select><br />
				<span class="description"><?php _e('The value affects the speed of import', 'ipytw'); ?>. <?php _e('If you are having trouble importing, try reduce the value in this field', 'ipytw'); ?>.</span>
			</td>
		 </tr>
		 <tr class="ipytw_tr">
			<th scope="row"><label for="ipytw_description_into"><?php _e('Description of products to import into', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<select name="ipytw_description_into" id="ipytw_description_into">						
					<option value="full" <?php selected($ipytw_description_into, 'full'); ?>><?php _e('Full description', 'ipytw'); ?></option>
					<option value="excerpt" <?php selected($ipytw_description_into, 'excerpt'); ?>><?php _e('Short description', 'ipytw'); ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="ipytw_source_sku"><?php _e('SKU', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<select name="ipytw_source_sku" id="ipytw_source_sku">
				<option value="disabled" <?php selected($ipytw_source_sku, 'disabled'); ?>><?php _e("Don't import", "ipytw"); ?></option>
				<option value="vendor_code" <?php selected($ipytw_source_sku, 'vendor_code'); ?>>vendorCode</option>
				<option value="shop_sku" <?php selected($ipytw_source_sku, 'shop_sku'); ?>>shop-sku</option>
				<option value="sku" <?php selected($ipytw_source_sku, 'sku'); ?>>sku</option>
				<option value="article" <?php selected($ipytw_source_sku, 'article'); ?>>article</option>
				<option value="product_id" <?php selected($ipytw_source_sku, 'product_id'); ?>><?php _e('Product ID', 'ipytw'); ?></option>
				<?php do_action('ipytw_source_sku_option', $feed_id, $ipytw_source_sku); ?>
				</select><br />
				<span class="description"><?php _e('Select the feed element, which contains the SKU', 'ipytw'); ?>.</span>
			</td>
		 </tr>
		 <tr>
			<th scope="row"><label for="ipytw_if_isset_sku"><?php _e('If there is a product on the site with the same SKU', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<select name="ipytw_if_isset_sku" id="ipytw_if_isset_sku">
				<option value="disabled" <?php selected($ipytw_if_isset_sku, 'disabled'); ?>><?php _e("Don't import", "ipytw"); ?></option>				
				<option value="without_sku" <?php selected($ipytw_if_isset_sku, 'without_sku'); ?>><?php _e('Import without SKU', 'ipytw'); ?></option>
				<option value="update" <?php selected($ipytw_if_isset_sku, 'update'); ?>><?php _e('Replace product on your site', 'ipytw'); ?></option>
				<?php do_action('ipytw_if_isset_sku_option', $feed_id, $ipytw_if_isset_sku); ?>
				</select><br />
				<span class="description"><?php _e('Choose what to do if, when importing products, an product with the same SKU as in the feed is found on your website', 'ipytw'); ?>.</span>
			</td>
		 </tr>
		 <tr>
			<th scope="row"><label for="ipytw_missing_product"><?php _e('If the product is not in the feed', 'ipytw'); ?></label><br /><i>(<?php _e('Important. The feature is under testing. Use it with care', 'ipytw'); ?></i>!)</th>
			<td class="overalldesc">
				<select name="ipytw_missing_product" id="ipytw_missing_product">
				<option value="disabled" <?php selected($ipytw_missing_product, 'disabled'); ?>><?php _e('Nothing', 'ipytw'); ?></option>				
				<option value="del" <?php selected($ipytw_missing_product, 'del'); ?>><?php _e('Delete', 'ipytw'); ?></option>
				<!-- option value="out_of_stock" <?php selected($ipytw_missing_product, 'out_of_stock'); ?>><?php _e('Not in stock', 'ipytw'); ?></option -->
				<?php do_action('ipytw_missing_product_option', $feed_id, $ipytw_missing_product); ?>
				</select><br />
				<span class="description"><?php _e('What if a previously imported product is missing from the feed', 'ipytw'); ?>.</span>
			</td>
		 </tr>
		 <tr class="ipytw_tr">
			<th scope="row"><label for="ipytw_fsize_limit"><?php _e('Skip images larger than', 'ipytw'); ?></label></th>
			<td class="overalldesc">
				<input type="number" step="1" min="1" max="20" name="ipytw_fsize_limit" id="ipytw_fsize_limit" value="<?php echo $ipytw_fsize_limit; ?>" placeholder="5" /><br />
				<span class="description"><?php _e('Skip images larger than', 'ipytw'); ?> (MB) <strong><i>(<?php _e("Default", "ipytw"); ?>: 5)</i><strong>.</span>
			</td>
		 </tr>
		</tbody></table>
	  </div>
	 </div>
	 <?php do_action('ipytw_before_button_primary_submit', $feed_id); ?>	 
	 <div class="postbox">
	  <div class="inside">
		<table class="form-table"><tbody>
		 <tr>
			<th scope="row"><label for="button-primary"></label></th>
			<td class="overalldesc"><?php wp_nonce_field('ipytw_nonce_action','ipytw_nonce_field'); ?><input id="button-primary" class="button-primary" type="submit" name="ipytw_submit_action" value="<?php _e( 'Save', 'ipytw'); ?>" /><br />
			<span class="description"><?php _e('Click to save the settings', 'ipytw'); ?></span></td>
		 </tr>
		</tbody></table>
	  </div>
	 </div>
	 <?php do_action('ipytw_append_form_container_2', $feed_id); ?>
	</form>
	<?php do_action('ipytw_append_container_2', $feed_id); ?>
  </div></div>
 </div><!-- /post-body --><br class="clear"></div><!-- /poststuff -->
 <?php do_action('ipytw_after_poststuff', $feed_id); ?>

 <div id="icp_slides" class="clear">
  <div class="icp_wrap">
	<input type="radio" name="icp_slides" id="icp_point1">
	<input type="radio" name="icp_slides" id="icp_point2">
	<input type="radio" name="icp_slides" id="icp_point3" checked>
	<input type="radio" name="icp_slides" id="icp_point4">
	<input type="radio" name="icp_slides" id="icp_point5">
	<input type="radio" name="icp_slides" id="icp_point6">
	<input type="radio" name="icp_slides" id="icp_point7">
	<div class="icp_slider">
		<div class="icp_slides icp_img1"><a href="//wordpress.org/plugins/import-from-yml/" target="_blank"></a></div>
		<div class="icp_slides icp_img2"><a href="//wordpress.org/plugins/import-products-to-ok-ru/" target="_blank"></a></div>
		<div class="icp_slides icp_img3"><a href="//wordpress.org/plugins/xml-for-google-merchant-center/" target="_blank"></a></div>
		<div class="icp_slides icp_img4"><a href="//wordpress.org/plugins/gift-upon-purchase-for-woocommerce/" target="_blank"></a></div>
		<div class="icp_slides icp_img5"><a href="//wordpress.org/plugins/xml-for-avito/" target="_blank"></a></div>
		<div class="icp_slides icp_img6"><a href="//wordpress.org/plugins/xml-for-o-yandex/" target="_blank"></a></div>
		<div class="icp_slides icp_img7"><a href="//wordpress.org/plugins/import-from-yml/" target="_blank"></a></div>
	</div>
	<div class="icp_control">
		<label for="icp_point1"></label>
		<label for="icp_point2"></label>
		<label for="icp_point3"></label>
		<label for="icp_point4"></label>
		<label for="icp_point5"></label>
		<label for="icp_point6"></label>
		<label for="icp_point7"></label>
	</div>
  </div> 
 </div>
 <?php do_action('ipytw_after_icp_slides', $feed_id); ?>

 <div class="metabox-holder">
  <div class="postbox">
  	<h2 class="hndle"><?php _e('My plugins that may interest you', 'ipytw'); ?></h2>
	<div class="inside">
		<p><span class="ipytw_bold">XML for Google Merchant Center</span> - <?php _e('Сreates a XML-feed to upload to Google Merchant Center', 'ipytw'); ?>. <a href="https://wordpress.org/plugins/xml-for-google-merchant-center/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p> 
		<p><span class="ipytw_bold">YML for Yandex Market</span> - <?php _e('Сreates a YML-feed for importing your products to Yandex Market', 'ipytw'); ?>. <a href="https://wordpress.org/plugins/import-from-yml/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p>
		<p><span class="ipytw_bold">Import from YML</span> - <?php _e('Imports products from YML to your shop', 'ipytw'); ?>. <a href="https://wordpress.org/plugins/import-from-yml/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p>
		<p><span class="ipytw_bold">XML for Hotline</span> - <?php _e('Сreates a XML-feed for importing your products to Hotline', 'ipytw'); ?>. <a href="https://wordpress.org/plugins/xml-for-hotline/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p>
		<p><span class="ipytw_bold">Gift upon purchase for WooCommerce</span> - <?php _e('This plugin will add a marketing tool that will allow you to give gifts to the buyer upon purchase', 'ipytw'); ?>. <a href="https://wordpress.org/plugins/gift-upon-purchase-for-woocommerce/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p>
		<p><span class="ipytw_bold">Import products to ok.ru</span> - <?php _e('With this plugin, you can import products to your group on ok.ru', 'ipytw'); ?>. <a href="https://wordpress.org/plugins/import-products-to-ok-ru/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p>
		<p><span class="ipytw_bold">XML for Avito</span> - <?php _e('Сreates a XML-feed for importing your products to', 'ipytw'); ?> Avito. <a href="https://wordpress.org/plugins/xml-for-avito/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p>
		<p><span class="ipytw_bold">XML for O.Yandex (Яндекс Объявления)</span> - <?php _e('Сreates a XML-feed for importing your products to', 'ipytw'); ?> Яндекс.Объявления. <a href="https://wordpress.org/plugins/xml-for-o-yandex/" target="_blank"><?php _e('Read more', 'ipytw'); ?></a>.</p>
	</div>
  </div>
 </div>
 <?php do_action('ipytw_append_wrap', $feed_id); ?>
</div><!-- /wrap -->
<?php
} /* end функция настроек ipytw_export_page */ ?>