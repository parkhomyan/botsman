<?php if (!defined('ABSPATH')) {exit;}
function ipytw_debug_page() { 
	wp_clean_plugins_cache();
	wp_clean_update_cache();
	add_filter('pre_site_transient_update_plugins', '__return_null');
	wp_update_plugins();
	remove_filter('pre_site_transient_update_plugins', '__return_null');
	if (isset($_REQUEST['ipytw_submit_debug_page'])) {
		if (!empty($_POST) && check_admin_referer('ipytw_nonce_action','ipytw_nonce_field')) {
			if (isset($_POST['ipytw_keeplogs'])) {
				ipytw_optionUPD('ipytw_keeplogs', sanitize_text_field($_POST['ipytw_keeplogs']));
				new IPYTW_Error_Log('NOTICE: Логи успешно включены; Файл: debug.php; Строка: '.__LINE__);
			} else {
				new IPYTW_Error_Log('NOTICE: Логи отключены; Файл: debug.php; Строка: '.__LINE__);
				ipytw_optionUPD('ipytw_keeplogs', '0');
			}	
			if (isset($_POST['ipytw_enable_backend_debug'])) {
				ipytw_optionUPD('ipytw_enable_backend_debug', sanitize_text_field($_POST['ipytw_enable_backend_debug']));
			} else {
				ipytw_optionUPD('ipytw_enable_backend_debug', '0');
			}
			if (isset($_POST['ipytw_disable_notices'])) {
				ipytw_optionUPD('ipytw_disable_notices', sanitize_text_field($_POST['ipytw_disable_notices']));
			} else {
				ipytw_optionUPD('ipytw_disable_notices', '0');
			}
			if (isset($_POST['ipytw_enable_five_min'])) {
				ipytw_optionUPD('ipytw_enable_five_min', sanitize_text_field($_POST['ipytw_enable_five_min']));
			} else {
				ipytw_optionUPD('ipytw_enable_five_min', '0');
			}		
		}
	}	
	$ipytw_keeplogs = ipytw_optionGET('ipytw_keeplogs');
	$ipytw_enable_backend_debug = ipytw_optionGET('ipytw_enable_backend_debug');
	$ipytw_disable_notices = ipytw_optionGET('ipytw_disable_notices');
	$ipytw_enable_five_min = ipytw_optionGET('ipytw_enable_five_min');
	?>
	<div class="wrap"><h1><?php _e('Debug page', 'ipytw'); ?> v.<?php echo ipytw_optionGET('ipytw_version'); ?></h1>
		<div id="dashboard-widgets-wrap"><div id="dashboard-widgets" class="metabox-holder">
			<div id="postbox-container-1" class="postbox-container"><div class="meta-box-sortables">
				<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">	 
				<div class="postbox">
					<h2 class="hndle"><?php _e('Logs', 'ipytw'); ?></h2>
					<div class="inside">
						<p><?php if ($ipytw_keeplogs === 'on') {
							$upload_dir = wp_get_upload_dir();
							echo '<strong>'. __("Log-file here", 'ipytw').':</strong><br /><a href="'.$upload_dir['baseurl'].'/import-from-yml/plugin.log" target="_blank">'.$upload_dir['basedir'].'/import-from-yml/plugin.log</a>';
						} ?></p>
						<table class="form-table"><tbody>
						<tr>
							<th scope="row"><label for="ipytw_keeplogs"><?php _e('Keep logs', 'ipytw'); ?></label><br />
								<input class="button" id="ipytw_submit_clear_logs" type="submit" name="ipytw_submit_clear_logs" value="<?php _e('Clear logs', 'ipytw'); ?>" />
							</th>
							<td class="overalldesc">
								<input type="checkbox" name="ipytw_keeplogs" id="ipytw_keeplogs" <?php checked($ipytw_keeplogs, 'on' ); ?>/><br />
								<span class="description"><?php _e('Do not check this box if you are not a developer', 'ipytw'); ?>!</span>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="ipytw_enable_backend_debug"><?php _e('Enable backend debugging', 'ipytw'); ?></label></th>
							<td class="overalldesc">
								<input type="checkbox" name="ipytw_enable_backend_debug" id="ipytw_enable_backend_debug" <?php checked($ipytw_enable_backend_debug, 'on' ); ?>/><br />
								<span class="description"><?php _e('Do not check this box if you are not a developer', 'ipytw'); ?>!</span>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="ipytw_disable_notices"><?php _e('Disable notices', 'ipytw'); ?></label></th>
							<td class="overalldesc">
								<input type="checkbox" name="ipytw_disable_notices" id="ipytw_disable_notices" <?php checked($ipytw_disable_notices, 'on' ); ?>/><br />
								<span class="description"><?php _e('Disable notices about products import', 'ipytw'); ?>!</span>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="ipytw_enable_five_min"><?php _e('Enable', 'ipytw'); ?> five_min</label></th>
							<td class="overalldesc">
								<input type="checkbox" name="ipytw_enable_five_min" id="ipytw_enable_five_min" <?php checked($ipytw_enable_five_min, 'on' ); ?>/><br />
								<span class="description"><?php _e('Enable the five minute interval for CRON', 'ipytw'); ?></span>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="button-primary"></label></th>
							<td class="overalldesc"></td>
						</tr>		 
						<tr>
							<th scope="row"><label for="button-primary"></label></th>
							<td class="overalldesc"><?php wp_nonce_field('ipytw_nonce_action', 'ipytw_nonce_field'); ?><input id="button-primary" class="button-primary" type="submit" name="ipytw_submit_debug_page" value="<?php _e( 'Save', 'ipytw'); ?>" /><br />
							<span class="description"><?php _e('Click to save the settings', 'ipytw'); ?></span></td>
						</tr>
						</tbody></table>
					</div>
				</div>
				</form> 
			</div></div>
			<div id="postbox-container-2" class="postbox-container"><div class="meta-box-sortables">
				<div class="postbox">
					<h2 class="hndle"><?php _e('Reset plugin settings', 'ipytw'); ?></h2>
					<div class="inside">
						<p><?php _e('Reset plugin settings can be useful in the event of a problem', 'ipytw'); ?>.</p>
						<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post" enctype="multipart/form-data">
							<?php wp_nonce_field('ipytw_nonce_action_reset', 'ipytw_nonce_field_reset'); ?><input class="button-primary" type="submit" name="ipytw_submit_reset" value="<?php _e('Reset plugin settings', 'ipytw'); ?>" />	 
						</form>
					</div>
				</div>
			</div></div>  
			<div id="postbox-container-3" class="postbox-container"><div class="meta-box-sortables">
			<div class="postbox">
				<h2 class="hndle"><?php _e('Possible problems', 'ipytw'); ?></h2>
				<div class="inside">
					<?php
						$possible_problems_arr = ipytw_possible_problems_list();
						if ($possible_problems_arr[1] > 0) { // $possibleProblemsCount > 0) {
							echo '<ol>'.$possible_problems_arr[0].'</ol>';
						} else {
							echo '<p>'. __('Self-diagnosis functions did not reveal potential problems', 'ipytw').'.</p>';
						}
					?>
				</div>
				</div>	  
				<div class="postbox">
				<h2 class="hndle"><?php _e('Sandbox', 'ipytw'); ?></h2>
				<div class="inside">	  	
						<?php
							require_once plugin_dir_path(__FILE__).'/sandbox.php';
							try {
								ipytw_run_sandbox();
							} catch (Exception $e) {
								echo 'Exception: ',  $e->getMessage(), "\n";
							}
						?>
					</div>
				</div>	  
			</div></div>  
			<div id="postbox-container-4" class="postbox-container"><div class="meta-box-sortables">
				<?php do_action('ipytw_before_support_project'); ?>
				<div class="postbox">
					<h2 class="hndle"><?php _e('Send data about the work of the plugin', 'ipytw'); ?></h2>
					<div class="inside">
						<p><?php _e('Sending statistics you help make the plugin even better', 'ipytw'); ?>! <?php _e('The following data will be transferred', 'ipytw'); ?>:</p>
						<ul class="ipytw_ul">
							<li>- <?php _e('Site URL', 'ipytw'); ?></li>
							<li>- <?php _e('URL YML-feed', 'ipytw'); ?></li>
							<li>- <?php _e('Is the multisite mode enabled', 'ipytw'); ?>?</li>
						</ul>
						<p><?php _e('Did the plugin help you import products into WooCommerce', 'ipytw'); ?>?</p>
						<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
						<p>
							<input type="radio" name="ipytw_its_ok" value="yes"><?php _e('Yes', 'ipytw'); ?><br />
							<input type="radio" name="ipytw_its_ok" value="no"><?php _e('No', 'ipytw'); ?><br />
						</p>
						<p><?php _e("If you don't mind to be contacted in case of problems, please enter your email address", "ipytw"); ?>. <span style="font-weight: 700;"><?php _e('And if you want a response, be sure to include your email address', 'ipytw'); ?></span>.</p>
						<p><input type="email" name="ipytw_email"></p>
						<p><?php _e("Your message", "ipytw"); ?>:</p>
						<p><textarea rows="5" cols="40" name="ipytw_message" placeholder="<?php _e('Enter your text to send me a message (You can write me in Russian or English). I check my email several times a day', 'ipytw'); ?>"></textarea></p>
						<?php wp_nonce_field('ipytw_nonce_action_send_stat', 'ipytw_nonce_field_send_stat'); ?><input class="button-primary" type="submit" name="ipytw_submit_send_stat" value="<?php _e('Send data', 'ipytw'); ?>" />
						</form>
					</div>
				</div>
			</div></div>
		</div></div>
	</div>
	<?php
} /* end функция страницы debug-а ipytw_debug_page */
?>