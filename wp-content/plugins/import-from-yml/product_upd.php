<?php if (!defined('ABSPATH')) {exit;}
include_once ABSPATH . 'wp-admin/includes/plugin.php'; // без этого не будет работать вне адмники is_plugin_active
function ipytw_product_upd($data, $feed_id) {
	new IPYTW_Error_Log('FEED № '.$feed_id.'; Стартовала ipytw_product_upd; Файл: product_upd.php; Строка: '.__LINE__);
	$ipytw_product_was_sync = $data['ipytw_product_was_sync'];
	$product = wc_get_product($data['site_product_id']);
	$price_element = 'price';
	$price_element = apply_filters('ipytw_price_element_filter', $price_element, $feed_id);

	$oldprice_element = 'oldprice';
	$oldprice_element = apply_filters('ipytw_oldprice_element_filter', $oldprice_element, $feed_id);
	switch ($ipytw_product_was_sync) {
		case 'whole': 
			$res = ipytw_product_add($data, $feed_id, $data['site_product_id']);
			return $res;
		break; 
		case 'price_only':
			$product_price = (float)$data['offer_xml_object']->$price_element;
			$product_price = apply_filters('ipytw_product_price_source_filter', $product_price, $data, $feed_id);
			$product_price = apply_filters('ipytw_product_price_filter', $product_price, $feed_id);
			$product->set_regular_price($product_price);	   
			$product_oldprice = (float)$data['offer_xml_object']->$oldprice_element;
			$product_oldprice = apply_filters('ipytw_product_oldprice_source_filter', $product_oldprice, $data, $feed_id);
			$product_oldprice = apply_filters('ipytw_product_oldprice_filter', $product_oldprice, $feed_id);
			if ($product_oldprice > (float)0) {  
				$product->set_sale_price($product_oldprice); 
			} else {
				$product->set_sale_price('');
			}		
		break; 
		case 'price_and_stock':
			$product_price = (float)$data['offer_xml_object']->$price_element;
			$product_price = apply_filters('ipytw_product_price_source_filter', $product_price, $data, $feed_id);
			$product_price = apply_filters('ipytw_product_price_filter', $product_price, $feed_id);
			$product->set_regular_price($product_price);	   
			$product_oldprice = (float)$data['offer_xml_object']->$oldprice_element;
			$product_oldprice = apply_filters('ipytw_product_oldprice_source_filter', $product_oldprice, $data, $feed_id);
			$product_oldprice = apply_filters('ipytw_product_oldprice_filter', $product_oldprice, $feed_id);
			if ($product_oldprice > (float)0) {  
				$product->set_sale_price($product_oldprice); 
			} else {
				$product->set_sale_price('');
			}
			$xml_offer_attr_object = $data['offer_xml_object']->attributes(); 
			if (property_exists($xml_offer_attr_object, 'available')) {
				$feed_product_available = (string)$xml_offer_attr_object->available;
				if ($feed_product_available === 'true') {$stock_status = 'instock';} else {$stock_status = 'outofstock';}
			} else {$stock_status = 'outofstock';}
			$product->set_stock_status($stock_status);	
			$product = apply_filters('ipytw_product_upd_case_price_and_stock_filter', $product, array('data' => $data), $feed_id);
		break;
		case 'dont_update': break;
		default:
			$product = apply_filters('ipytw_product_upd_case_default_filter', $product, array('ipytw_product_was_sync' => $ipytw_product_was_sync, 'data' => $data), $feed_id);
	}
	$unixtime = current_time('Y-m-d H:i');
	update_post_meta($data['site_product_id'], '_ipytw_date_last_import', $unixtime);
	$ipytw_post_status = ipytw_optionGET('ipytw_post_status', $feed_id, 'set_arr');	
	$product->set_status($ipytw_post_status);
	$product = apply_filters('ipytw_product_upd_before_save_filter', $product, array('data' => $data), $feed_id);
	$product_id = $product->save();

	$product_price = $product->get_price(); /* ! */
	$imported_ids = array();
	$imported_ids['feed_product_id'] = $data['feed_product_id'];
	$imported_ids['site_product_id'] = $data['site_product_id'];
	$imported_ids['feed_product_cat'] = '';
	$imported_ids['site_product_cat'] = '';
	$imported_ids['product_price'] = $product_price;
	unset($product);
	return $imported_ids;
} // end function product_upd($data, $feed_id) {
?>