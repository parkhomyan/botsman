<?php if (!defined('ABSPATH')) {exit;}
/*
* @since 1.0.0
*
* @param string $option_name (require)
* @param string $value (require)
* @param string $n (not require)
* @param string $autoload (not require)
* @param string $type (not require) (@since 1.3.0)
* @param string $source_settings_name (not require) (@since 1.3.0)
*
* @return true/false
* Возвращает то, что может быть результатом add_blog_option, add_option
*/
function ipytw_optionADD($option_name, $value = '', $n = '', $autoload = 'yes', $type = 'option', $source_settings_name = '') {
	if ($option_name == '') {return false;}
	switch ($type) {
		case "set_arr":
			if ($option_name === 'ipytw_status_sborki' || $option_name === 'ipytw_last_element') {if ($n === '0' || $n === 0) {$n = '';}}
			$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
			$ipytw_settings_arr[$n][$option_name] = $value;
			if (is_multisite()) { 
				return update_blog_option(get_current_blog_id(), 'ipytw_settings_arr', $ipytw_settings_arr);
			} else {
				return update_option('ipytw_settings_arr', $ipytw_settings_arr, $autoload);
			}
		break;
		case "custom_set_arr":
			if ($source_settings_name === '') {return false;}
			if ($n === '0' || $n === 0) {$n = '';}
			$ipytw_settings_arr = ipytw_optionGET($source_settings_name);
			$ipytw_settings_arr[$n][$option_name] = $value;
			if (is_multisite()) { 
				return update_blog_option(get_current_blog_id(), $source_settings_name, $ipytw_settings_arr);
			} else {
				return update_option($source_settings_name, $ipytw_settings_arr, $autoload);
			}
		break;
		default:
			if ($n === '0' || $n === 0) {$n = '';}
			$option_name = $option_name.$n;
			if (is_multisite()) { 
				return add_blog_option(get_current_blog_id(), $option_name, $value);
			} else {
				return add_option($option_name, $value, '', $autoload);
			}
	}
}
/*
* @since 1.0.0
*
* @param string $option_name (require)
* @param string $value (require)
* @param string $n (not require)
* @param string $autoload (not require)
* @param string $type (not require) (@since 1.3.0)
* @param string $source_settings_name (not require) (@since 1.3.0)
*
* @return true/false
* Возвращает то, что может быть результатом update_blog_option, update_option
*/
function ipytw_optionUPD($option_name, $value = '', $n = '', $autoload = 'yes', $type = '', $source_settings_name = '') {
	if ($option_name == '') {return false;}
	switch ($type) {
		case "set_arr": 
			if ($option_name === 'ipytw_status_sborki' || $option_name === 'ipytw_last_element') {if ($n === '0' || $n === 0) {$n = '';}}
			$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
			$ipytw_settings_arr[$n][$option_name] = $value;
			if (is_multisite()) { 
				return update_blog_option(get_current_blog_id(), 'ipytw_settings_arr', $ipytw_settings_arr);
			} else {
				return update_option('ipytw_settings_arr', $ipytw_settings_arr, $autoload);
			}
		break;
		case "custom_set_arr": 
			if ($source_settings_name === '') {return false;}
			if ($n === '0' || $n === 0) {$n = '';}
			$ipytw_settings_arr = ipytw_optionGET($source_settings_name);
			$ipytw_settings_arr[$n][$option_name] = $value;
			if (is_multisite()) { 
				return update_blog_option(get_current_blog_id(), $source_settings_name, $ipytw_settings_arr);
			} else {
				return update_option($source_settings_name, $ipytw_settings_arr, $autoload);
			}
		break;
		default:
			if ($n === '0' || $n === 0) {$n = '';}
			$option_name = $option_name.$n;
			if (is_multisite()) { 
				return update_blog_option(get_current_blog_id(), $option_name, $value);
			} else {
				return update_option($option_name, $value, $autoload);
			}
	}
}
/*
* @since 1.0.0
*
* @param string $optName (require)
* @param string $n (not require)
* @param string $type (not require) (@since 1.3.0)
* @param string $source_settings_name (not require) (@since 1.3.0)
*
* @return Значение опции (string/array) или false
* Возвращает то, что может быть результатом get_blog_option, get_option
*/
function ipytw_optionGET($option_name, $n = '', $type = '', $source_settings_name = '') {
	if ($option_name == '') {return false;}	
	switch ($type) {
		case "set_arr": 
			if ($option_name === 'ipytw_status_sborki' || $option_name === 'ipytw_last_element') {if ($n === '0' || $n === 0) {$n = '';}}
			$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
			if (isset($ipytw_settings_arr[$n][$option_name])) {
				return $ipytw_settings_arr[$n][$option_name];
			} else {
				return false;
			}
		break;
		case "custom_set_arr":
			if ($source_settings_name === '') {return false;}
			if ($n === '0' || $n === 0) {$n = '';}
			$ipytw_settings_arr = ipytw_optionGET($source_settings_name);
			if (isset($ipytw_settings_arr[$n][$option_name])) {
				return $ipytw_settings_arr[$n][$option_name];
			} else {
				return false;
			}
		break;
		case "for_update_option":
			if ($n === '0' || $n === 0) {$n = '';}
			$option_name = $option_name.$n;
			if (is_multisite()) { 
				return get_blog_option(get_current_blog_id(), $option_name);
			} else {
				return get_option($option_name);
			}		
		break;
		default:
			if ($n === '0' || $n === 0) {$n = '';}
			$option_name = $option_name.$n;
			if (is_multisite()) { 
				return get_blog_option(get_current_blog_id(), $option_name);
			} else {
				return get_option($option_name);
			}
	}
}
/*
* С версии 1.0.0
*
* @param string $option_name (require)
* @param string $n (not require)
* @param string $type (not require) (@since 1.3.0)
* @param string $source_settings_name (not require) (@since 1.3.0)
*
* @return true/false
* Возвращает то, что может быть результатом delete_blog_option, delete_option
*/
function ipytw_optionDEL($option_name, $n = '', $type = '', $source_settings_name = '') {
	if ($option_name == '') {return false;}	 
	switch ($type) {
		case "set_arr": 
			if ($option_name === 'ipytw_status_sborki' || $option_name === 'ipytw_last_element') {if ($n === '0' || $n === 0) {$n = '';}}
			$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
			unset($ipytw_settings_arr[$n][$option_name]);
			if (is_multisite()) { 
				return update_blog_option(get_current_blog_id(), 'ipytw_settings_arr', $ipytw_settings_arr);
			} else {
				return update_option('ipytw_settings_arr', $ipytw_settings_arr);
			}
		break;
		case "custom_set_arr": 
			if ($source_settings_name === '') {return false;}
			if ($n === '0' || $n === 0) {$n = '';}
			$ipytw_settings_arr = ipytw_optionGET($source_settings_name);
			unset($ipytw_settings_arr[$n][$option_name]);
			if (is_multisite()) { 
				return update_blog_option(get_current_blog_id(), $source_settings_name, $ipytw_settings_arr);
			} else {
				return update_option($source_settings_name, $ipytw_settings_arr);
			}
		break;
		default:
		if ($n === '0' || $n === 0) {$n = '';}
		$option_name = $option_name.$n;
		if (is_multisite()) { 
			return delete_blog_option(get_current_blog_id(), $option_name);
		} else {
			return delete_option($option_name);
		}
	}
}
/*
* @since 1.0.0
*
* @param string $name_dir (require)
*
* @return true/false
* Проверяет наличие и при необходимости создаёт папку кэша плагина
*/
function ipytw_check_dir($name_dir) {
	if (!is_dir($name_dir)) {
		if (!mkdir($name_dir)) {
			error_log('ERROR: Нет папки '.$name_dir.'! И создать не вышло! $name_dir ='.$name_dir.'; Файл: functions.php; Строка: '.__LINE__, 0);
			return false;
		} else {
			error_log('ERROR: Создали папку '.$name_dir.'! Файл: functions.php; Строка: '.__LINE__, 0);
			return true;
		}
	} return true;
}
/*
* @since 1.0.0
*
* @param string $url (require)
*
* @return nothing
* Записывает текст ошибки, чтобы потом можно было отправить в отчет
*/
function ipytw_errors_log($message) {
	$message = '['.date('Y-m-d H:i:s').'] '. $message;
	if (is_multisite()) {
		update_blog_option(get_current_blog_id(), 'ipytw_errors', $message);
	} else {
		update_option('ipytw_errors', $message, '', 'no');
	}
}
/*
* @since 1.0.0
*
* @return array
* Возвращает список потенциальных проблем
*/
function ipytw_possible_problems_list() {
	$possibleProblems = ''; $possibleProblemsCount = 0; $conflictWithPlugins = 0; $conflictWithPluginsList = ''; 
	$check_global_attr_count = wc_get_attribute_taxonomies();
	if (count($check_global_attr_count) < 1) {
		$possibleProblemsCount++;
		$possibleProblems .= '<li>'. __('Your site has no global attributes! This may affect the quality of the YML feed. This can also cause difficulties when setting up the plugin', 'ipytw'). '. <a href="https://icopydoc.ru/globalnyj-i-lokalnyj-atributy-v-woocommerce/?utm_source=yml-for-yandex-market&utm_medium=organic&utm_campaign=in-plugin-yml-for-yandex-market&utm_content=debug-page&utm_term=possible-problems">'. __('Please read the recommendations', 'ipytw'). '</a>.</li>';
	}
	if (is_plugin_active('snow-storm/snow-storm.php')) {
		$possibleProblemsCount++;
		$conflictWithPlugins++;
		$conflictWithPluginsList .= 'Snow Storm<br/>';
	}
	if (is_plugin_active('email-subscribers/email-subscribers.php')) {
		$possibleProblemsCount++;
		$conflictWithPlugins++;
		$conflictWithPluginsList .= 'Email Subscribers & Newsletters<br/>';
	}
	if (is_plugin_active('saphali-search-castom-filds/saphali-search-castom-filds.php')) {
		$possibleProblemsCount++;
		$conflictWithPlugins++;
		$conflictWithPluginsList .= 'Email Subscribers & Newsletters<br/>';
	}
	if (is_plugin_active('w3-total-cache/w3-total-cache.php')) {
		$possibleProblemsCount++;
		$conflictWithPlugins++;
		$conflictWithPluginsList .= 'W3 Total Cache<br/>';
	}
	if (is_plugin_active('docket-cache/docket-cache.php')) {
		$possibleProblemsCount++;
		$conflictWithPlugins++;
		$conflictWithPluginsList .= 'Docket Cache<br/>';
	}					
	if (class_exists('MPSUM_Updates_Manager')) {
		$possibleProblemsCount++;
		$conflictWithPlugins++;
		$conflictWithPluginsList .= 'Easy Updates Manager<br/>';
	}
	if (class_exists('OS_Disable_WordPress_Updates')) {
		$possibleProblemsCount++;
		$conflictWithPlugins++;
		$conflictWithPluginsList .= 'Disable All WordPress Updates<br/>';
	}
	if ($conflictWithPlugins > 0) {
		$possibleProblemsCount++;
		$possibleProblems .= '<li><p>'. __('Most likely, these plugins negatively affect the operation of', 'ipytw'). ' Import products from YML to WooCommerce:</p>'.$conflictWithPluginsList.'<p>'. __('If you are a developer of one of the plugins from the list above, please contact me', 'ipytw').': <a href="mailto:support@icopydoc.ru">support@icopydoc.ru</a>.</p></li>';
	}
	return array($possibleProblems, $possibleProblemsCount, $conflictWithPlugins, $conflictWithPluginsList);
}
/*
* С версии 1.0.0
*
* @param int $feedId (require)
*
* @return array
* Возвращает массив просинхронизированных категорий. Где ключ - id массива в фиде. Значение - id категори у нас на сайте
*/
function ipytw_get_imported_cat($feedId) {
	new IPYTW_Error_Log('FEED № '.$feedId.'; Стартовала ipytw_get_imported_cat; Файл: functions.php; Строка: '.__LINE__);

	$upload_dir = (object)wp_get_upload_dir();
	$name_dir = $upload_dir->basedir.'/import-from-yml';
	ipytw_check_dir($name_dir);

	$filename = $name_dir.'/feed-importetd-cat-ids-'.$feedId.'.tmp';

	if (is_file($filename)) {
		$result_arr = file_get_contents($filename);
		return unserialize($result_arr);
	} else { // Файла нет
		new IPYTW_Error_Log('FEED № '.$feedId.'; ERROR: Нет файла: '.$filename.'; Файл: functions.php; Строка: '.__LINE__);
		return array();
	}
} // end function ipytw_get_imported_cat()
/*
* @since 1.0.0
*
* @param string $meta_value (require)
* @param int $feedId (require) - @since 1.1.0
* @param string $meta_key (not require)
* @param string $post_type (not require)
* @param bool $product_sku (not require) - @since 1.2.2
*
* @return int/false
* Проверяет, синхронили ли мы товар/картинку
*/
function ipytw_check_sync($meta_value, $feedId, $post_type = 'attachment', $meta_key = '_ipytw_import_feed_picture_url', $product_sku = false) {
	if ($post_type === 'product') {
		$meta_key = '_ipytw_feed_product_id'; 
		$args = array(
			'post_type' => $post_type,
			'fields' => 'ids',
			'relation' => 'AND',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => $meta_key,
					'value' => $meta_value,
				),
				array(
					'key' => '_ipytw_feed_id',
					'value' => $feedId,
				),
			),
		);
	} else {
		$args = array(
			'post_type' => $post_type,
			'fields' => 'ids',
			'relation' => 'AND',
			'meta_query' => array(
				array(
					'key' => $meta_key,
					'value' => $meta_value,
				),
			),
		);
	}
	if ($ids = get_posts($args)) {
		$result = array_pop($ids);
	} else {
		$result = false;
		/* if ($product_sku === false) {
			$result = false;
		} else {
			if ($post_type === 'product') {
				$isset_product_id = wc_get_product_id_by_sku($product_sku);
				if ($isset_product_id > 0) {
					$result = $isset_product_id;
					new IPYTW_Error_Log('NOTICE: Артикул '.$product_sku.' обнаружен у товара с $isset_product_id = '.$isset_product_id.'; Файл: function.php; Строка: '.__LINE__);
				} else {
					$result = false;
				}
			} else {
				$result = false;
			}
		} */
	}
	return $result;
}
/*
* @since 1.1.1
*
* @param string $name (require)
*
* @return int/false
* Проверяет, существует ли в системе глобальный атрибут. Если не существует - создаёт
* https://stackoverflow.com/questions/29549525/create-new-product-attribute-programmatically-in-woocommerce/51994543#51994543
* https://stackoverflow.com/questions/58942156/woocommerce-programmatically-add-product-attributes-and-their-corresponding-valu
*/
function ipytw_create_product_attribute($attribute_name) {
	// $singular_name - для наглядности ибо get_taxonomy() 
	// ["name"] => string(24) "pa_второй-тест" 
	// ["singular_name"] => string(21) "Второй тест" 
	new IPYTW_Error_Log('INFO: Стартавала ipytw_create_product_attribute. $attribute_name = '.$attribute_name.'; Файл: function.php; Строка: '.__LINE__);
	if (function_exists('wc_get_attribute_taxonomies')) {
		$attribute_taxonomies = wc_get_attribute_taxonomies();
	} else {
		global $woocommerce; $attribute_taxonomies = $woocommerce->get_attribute_taxonomies();
	}
	$attribute_id = 0;
	if (count($attribute_taxonomies) > 0) {
		foreach($attribute_taxonomies as $one_tax ) {
			/**
			* $one_tax->attribute_id => 6
			* $one_tax->attribute_name] => слаг (на инглише или русском)
			* $one_tax->attribute_label] => Еще один атрибут (это как раз название)
			* $one_tax->attribute_type] => select 
			* $one_tax->attribute_orderby] => menu_order
			* $one_tax->attribute_public] => 0			
			*/
			if ($one_tax->attribute_label == $attribute_name) {
				$attribute_id = (int)$one_tax->attribute_id;
				$taxonomy_name = $one_tax->attribute_name; 
				new IPYTW_Error_Log('Есть глобальный атрибут "'.$attribute_name.'" с $taxonomy_name = '.$taxonomy_name.', $attribute_id = '.$attribute_id.'; Файл: function.php; Строка: '.__LINE__);
				break;
			}
		}
	}
	if ($attribute_id === 0) {
		new IPYTW_Error_Log('Нет глобального атрибута $attribute_name = '.$attribute_name.'; Файл: function.php; Строка: '.__LINE__);
	} else {
		new IPYTW_Error_Log('INFO: Перед return1; $attribute_id = '.$attribute_id.' gettype('.gettype($attribute_id).'); Файл: function.php; Строка: '.__LINE__);
		return $attribute_id;
	}

	$slug = wc_sanitize_taxonomy_name($attribute_name); // приводим к виду второй-тест
	if (strlen($slug) >= 28) {$slug = ipytw_strlen_slug($slug);} 

	if (wc_check_if_attribute_name_is_reserved($slug)) {
		new IPYTW_Error_Log('ERROR: Ошибка создания атрибута. Название таксономии $slug = '.$slug.' зарезервировано; Файл: function.php; Строка: '.__LINE__);
		return false;
	}

	$attribute_id = wc_create_attribute(array(
		'name'			=> $attribute_name,
		'slug'			=> $slug,
		'type'			=> 'select',
		'order_by'		=> 'menu_order',
		'has_archives'	=> false, // Enable archives ==> true
	));

	if (is_wp_error($attribute_id)) {
		$error_message = $attribute_id->get_error_message();
		$error_code = $attribute_id->get_error_code();
		new IPYTW_Error_Log('ERROR: Ошибка создания атрибута $attribute_name = '.$attribute_name.'; $error_message = '.$error_message.'; $error_code = '.$error_code.'; Файл: function.php; Строка: '.__LINE__);
		return false;
	} else {
		new IPYTW_Error_Log('INFO: Глобальный атрибут '.$attribute_name.'; $attribute_id = '.$attribute_id.' успешно создан; Файл: function.php; Строка: '.__LINE__);
	}

	// Register it as a wordpress taxonomy for just this session. Later on this will be loaded from the woocommerce taxonomy table.
	register_taxonomy(
		$slug,
		apply_filters('woocommerce_taxonomy_objects_' . $slug, array('product')),
		apply_filters('woocommerce_taxonomy_args_' . $slug, array(
			'labels' => array('name' => $attribute_name,),
			'hierarchical'	=> true,
			'show_ui'		=> false,
			'query_var'		=> true,
			'rewrite'		=> false,
		))
	);

	// do_action('woocommerce_attribute_added', $id, $data);
	wp_schedule_single_event( time(), 'woocommerce_flush_rewrite_rules');
	delete_transient('wc_attribute_taxonomies'); // Clear caches

	new IPYTW_Error_Log('INFO: Перед return2; $attribute_id = '.$attribute_id.' gettype('.gettype($attribute_id).'); Файл: function.php; Строка: '.__LINE__);
	return $attribute_id;
}
/*
* @since 1.1.1
*
* @param string $attribute_value (require)
* @param int $attribute_id (not require)
* @param string $attribute_taxonomy (not require)
*
* @return array(2) (["id"]=> int ["slug"]=> string) / array(2) (["id"]=> false ["slug"]=> false)
* Проверяет, существует ли значение у данного глобального атрибута. Если не существует - создаёт
* https://stackoverflow.com/questions/29549525/create-new-product-attribute-programmatically-in-woocommerce/51994543#51994543
* https://stackoverflow.com/questions/58942156/woocommerce-programmatically-add-product-attributes-and-their-corresponding-valu
*/
function ipytw_create_attribute_term($attribute_value, $attribute_id = 0 /*, $attribute_taxonomy = ''*/) {
	if ($attribute_id === 0) {return array('id' => false, 'slug' => false);}
	$attribute_id = (int)$attribute_id;

	$attribute_taxonomy_name = wc_attribute_taxonomy_name_by_id($attribute_id);

	// Look if there is already a term for this attribute?
	$term = get_term_by('name', $attribute_value, $attribute_taxonomy_name);

	if (!$term) { //No, create new term.
		$term = wp_insert_term($attribute_value, $attribute_taxonomy_name);
		if (is_wp_error($term))	{
			new IPYTW_Error_Log('ERROR: Невозможно создать новый термин атрибута $attribute_value = '.$attribute_value.' таксономии $attribute_taxonomy_name = '.$attribute_taxonomy_name.'; Файл: function.php; Строка: '.__LINE__);
			$error_message = $term->get_error_message();
			$error_code = $term->get_error_code();
			new IPYTW_Error_Log('ERROR: Ошибка создания термина атрибута $attribute_value = '.$attribute_value.'; $error_message = '.$error_message.'; $error_code = '.$error_code.'; Файл: function.php; Строка: '.__LINE__);
			return array('id' => false, 'slug' => false);
		}
		$termId = $term['term_id'];
		$term_slug = get_term($termId, $attribute_taxonomy_name)->slug; // Get the term slug
	} else {
		// Yes, grab it's id and slug
		$termId = $term->term_id;
		$term_slug = $term->slug;
	}
	return array('id' => $termId, 'slug' => $term_slug);
}
/*
* @since 1.1.1
*
* @param string $attributes (require) ($attributes = $product->get_attributes();)
* @param string $attribute_name (require)
*
* @return true/false
* Проверяет, существует ли атрибут с таким название у товара
*/
function ipytw_has_attribute($attributes, $attribute_name) {
	foreach ($attributes as $param) {					
		$param_name = wc_attribute_label(wc_attribute_taxonomy_name_by_id($param->get_id()));
		if ($param_name == $attribute_name) { 
			return true;
		}
	}
	return false;
}
/*
* @since 1.2.0
*
* @param string $path (require)
* @param string $unit (not require)
* @param int $round (not require)
* @param string $show_unit (not require)
* @param string $dec_point (not require)
* @param string $thousands_sep (not require)
*
* @return int/null
* Возвращает размер файла на удалённом сервере
* https://yandex.ru/turbo/internet-technologies.ru/s/articles/opredelenie-razmera-udalennogo-fayla.html
*/
function ipytw_fsize($path, $unit = 'B', $show_unit = 'no', $round = 2, $dec_point = '', $thousands_sep = '') {
	$result = null;
	if (empty($path)) {return $result;}
	$fp = fopen($path, "r");
	$inf = stream_get_meta_data($fp);
	fclose($fp);
	foreach ($inf["wrapper_data"] as $v) {
		if (stristr($v, "content-length")) {
			$v = explode(":", $v);
			$result = trim($v[1]);
			switch ($unit) {
				case 'GB': $result = number_format($result / 1073741824, $round, $dec_point, $thousands_sep); break;
				case 'MB': $result = number_format($result / 1048576, $round, $dec_point, $thousands_sep); break;
				case 'KB': $result = number_format($result / 1024, $round, $dec_point, $thousands_sep); break;
				case 'B': $result = number_format($result / 1, $round, $dec_point, $thousands_sep); break;
				default: $result = number_format($result / 1, $round, $dec_point, $thousands_sep);
			}	
			if ($show_unit === 'yes') {$result = $result.' '.$unit;}
			break;
		} 
	}
	return $result;
}
/*
* @since 1.2.2
*
* @param string $sku (require)
*
* @return true/false
* Проверяет, есть ли товар с таким SKU
* https://woocommerce.github.io/code-reference/files/woocommerce-includes-wc-product-functions.html
*/
function ipytw_is_uniq_sku($sku) {
	if ($sku === false || $sku === '') {return false;}
	if (wc_get_product_id_by_sku($sku) > 0) {
		return true;
	} else {
		return false;
	}
}
/*
* @since 1.2.3
*
* @param string $slug (require)
*
* @return string
*/
function ipytw_strlen_slug($slug) {
	$slug = mb_substr($slug, 0, 27); // 27 символом на слаг
	if (strlen($slug) >= 28) {	// если в итоге байт больше
		for ($i = mb_strlen($slug); $i = 1; $i--) {
			$slug = mb_substr($slug, 0, -1);
			if (strlen($slug) < 28) {break;}
		}
	}
	return $slug;
}
/*
* @since 1.3.0
*
* @return (string) feed ID or (string)''
* Получает первый фид. Используется на случай если get-параметр numFeed не указан
*/
function get_first_feed_id() {
	$ipytw_settings_arr = ipytw_optionGET('ipytw_settings_arr');
	if (!empty($ipytw_settings_arr)) {
		$v = array_key_first($ipytw_settings_arr);
		// удалим фид с нулевым id (актуально для перехода со старых версий)
		if ($v == '0') {
			ipytw_del_feed_zero();
			return get_first_feed_id();
		}
		// end удалим фид с нулевым id (актуально для перехода со старых версий)
		return $v;
	} else {
		return '';
	}
}
/*
* @since 1.3.0
*
* @param string $url (require)
*
* Вместо file_get_contents используем curl
*/
function ipytw_file_get_contents_curl($url) {
	$ch = curl_init(); // Инициализируем curl

	curl_setopt($ch, CURLOPT_URL, $url ); // Указываем ссылку
	curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE); // подставляем Referer при перенаправлении
	curl_setopt($ch, CURLOPT_HEADER, 0); // Не выводим заголовки
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Сохраним полученный результат в переменную 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); // Идем по перенаправлению 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Не проверяем SSL сертификат
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0'); // Установим Useragent

	$getData = curl_exec($ch); // Выполняем запрос на сайт
	curl_close($ch); // Закрываем curl

	return $getData; // Возвращаем из функции полученные данные
}
/*
* @since 1.5.2
*
*/
function ipytw_wf($feedId, $imported_ids_arr) {
	$upload_dir = (object)wp_get_upload_dir();
	$name_dir = $upload_dir->basedir."/import-from-yml";
	if (!is_dir($name_dir)) {
		new IPYTW_Error_Log('WARNING: Папка $name_dir ='.$name_dir.' нет; Файл: functions.php; Строка: '.__LINE__);
		if (!mkdir($name_dir)) {
			new IPYTW_Error_Log('ERROR: Создать папку $name_dir ='.$name_dir.' не вышло; Файл: functions.php; Строка: '.__LINE__);
		} else { 
			// if (ipytw_optionGET('yzen_yandex_zen_rss') == 'enabled') {$result_yml = ipytw_optionGET('ipytw_feed_content');};
		}
	} else {
		// if (ipytw_optionGET('yzen_yandex_zen_rss') == 'enabled') {$result_yml = ipytw_optionGET('ipytw_feed_content');};
	}

	if (is_dir($name_dir)) {  
		$filename = $name_dir.'/feed-importetd-product-ids-new-'.$feedId.'.tmp';
		$content = $imported_ids_arr['feed_product_id'].'-fisi-'.$imported_ids_arr['site_product_id'].PHP_EOL;
		file_put_contents($filename, $content, FILE_APPEND);
		/*$fp = fopen($filename, "w");
		fwrite($fp, $imported_ids);
		fclose($fp);*/
	} else {
		new IPYTW_Error_Log('ERROR: Нет папки import-from-yml! $name_dir ='.$name_dir.'; Файл: functions.php; Строка: '.__LINE__);
	}
}
/*
* @since 1.5.4
*
* @param string $slug (require)
*
* @return true/false
* Создаёт копию фида-источника на нашем сервере
*/
function copy_feed_from_source($feedId) {
	$ipytw_url_yml_file = ipytw_optionGET('ipytw_url_yml_file', $feedId, 'set_arr');
	if ($ipytw_url_yml_file == '') {
		new IPYTW_Error_Log('FEED № '.$feedId.'; ERROR: Не указан URL фида: '.$ipytw_url_yml_file.'. Остановим сборку; Файл: offer.php; Строка: '.__LINE__); 
		return false;
	}
	$xml_string = ipytw_file_get_contents_curl($ipytw_url_yml_file);
	if ($xml_string === false) { 
		new IPYTW_Error_Log('ERROR: copy_feed: Нет доступа к файлу $ipytw_url_yml_file = '.$ipytw_url_yml_file.'; Файл: offer.php; Строка: '.__LINE__);
		return false;
	} else {	
		if (is_dir(IPYTW_PLUGIN_UPLOADS_DIR_PATH)) {
			$filename = IPYTW_PLUGIN_UPLOADS_DIR_PATH.'/'.$feedId.'.xml';
			$fp = fopen($filename, "w");
			fwrite($fp, $xml_string); // записываем в файл текст
			fclose($fp); // закрываем
			ipytw_optionUPD('ipytw_copy_feed_from_source', $filename, $feedId, 'yes', 'set_arr');
			return true;
		} else {
			new IPYTW_Error_Log('ERROR: Нет папки '.IPYTW_PLUGIN_UPLOADS_DIR_PATH .'; Файл: functions.php; Строка: '.__LINE__);
			return false;
		}
	}	
}
?>