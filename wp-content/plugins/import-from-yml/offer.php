<?php if (!defined('ABSPATH')) {exit;}
include_once ABSPATH . 'wp-admin/includes/plugin.php'; // без этого не будет работать вне адмники is_plugin_active
function ipytw_unit($offer_xml_object, $feed_imported_cat_arr, $feedId, $feed_imported_offers_arr = array()) {	
 // https://skosta.ru/wordpress/woocommerce/
 // https://www.php.net/manual/ru/class.simplexmlelement.php
 // https://wp-kama.ru/plugin/woocommerce/function/WC_Product
 // http://wpcommerce.ru/threads/kak-programmno-dobavit-variaciju-k-tovaru.5919/
 new IPYTW_Error_Log('FEED № '.$feedId.'; Стартовала ipytw_unit; Файл: offer.php; Строка: '.__LINE__);
 $stop_flag = false;
 $xml_offer_attr_object = $offer_xml_object->attributes();
 if ($xml_offer_attr_object->group_id === null) {
 	$feed_product_id = (string)$xml_offer_attr_object->id;
 } else {
	new IPYTW_Error_Log('NOTICE: Пропускаем вариативный товар; Файл: offer.php; Строка: '.__LINE__);
	return false;
 }
 new IPYTW_Error_Log('INFO: Проверяем товар. Его id в фиде $feed_product_id = '.$feed_product_id.'; Файл: offer.php; Строка: '.__LINE__);

 $stop_flag = apply_filters('ipytw_stop_flag_filter', $stop_flag, array('offer_xml_object' => $offer_xml_object, 'feed_imported_cat_arr' => $feed_imported_cat_arr), $feedId);
 if ($stop_flag === true) {
	new IPYTW_Error_Log('NOTICE: Пропускаем товар $feed_product_id = '.$feed_product_id.' по флагу; Файл: offer.php; Строка: '.__LINE__);
	return false;
 }

 $ipytw_source_sku = ipytw_optionGET('ipytw_source_sku', $feedId, 'set_arr'); 
 switch ($ipytw_source_sku) {
	case 'vendor_code':
		$product_sku = (string)$offer_xml_object->vendorCode;
	break;
	case 'shop_sku':
		$el_name = 'shop-sku';
		$product_sku = (string)$offer_xml_object->$el_name;
	break;
	case 'sku':
		$product_sku = (string)$offer_xml_object->sku;		
	break;
	case 'article':
		$product_sku = (string)$offer_xml_object->article;		
	break;
	case 'product_id':
		$product_sku = (string)$feed_product_id;
	break;
	default:
		$product_sku = false;
		do_action('ipytw_switch_source_check_sku_action', $offer_xml_object, $feedId); 
 }
 // проверяем, существует ли товар с таким артикулом
 if (ipytw_is_uniq_sku($product_sku) === true) {
	new IPYTW_Error_Log('FEED № '.$feedId.'; Товар с артикулом $product_sku = '.$product_sku.' уже существует; Файл: offer.php; Строка: '.__LINE__);
	$ipytw_if_isset_sku = ipytw_optionGET('ipytw_if_isset_sku', $feedId, 'set_arr');
	if ($ipytw_if_isset_sku === 'disabled' || $ipytw_if_isset_sku === '') {
		new IPYTW_Error_Log('FEED № '.$feedId.'; WARNING: Товар пропущен по причине дублирующегося артикула; Файл: offer.php; Строка: '.__LINE__);
		return false;
	}
	if ($ipytw_if_isset_sku === 'update') {
		$check_sync_product = (int)wc_get_product_id_by_sku($product_sku);
	} else {
		$check_sync_product = ipytw_check_sync($feed_product_id, $feedId, 'product');
	}
 } else {
 	$check_sync_product = ipytw_check_sync($feed_product_id, $feedId, 'product');
 }
 if ($check_sync_product === false) {
	new IPYTW_Error_Log('FEED № '.$feedId.'; Товар ранее не был импортирован; Файл: offer.php; Строка: '.__LINE__);
	$args = array(
		'feed_product_id' => $feed_product_id,
		'offer_xml_object' => $offer_xml_object,
		'feed_imported_cat_arr' => $feed_imported_cat_arr,
	);
	$res = ipytw_product_add($args, $feedId);
 } else {
	$site_product_id = (int)$check_sync_product;
	new IPYTW_Error_Log('FEED № '.$feedId.'; Товар ранее был импортирован. $site_product_id = '.$site_product_id.'; Файл: offer.php; Строка: '.__LINE__);
	$args = array(
		'feed_product_id' => $feed_product_id,
		'offer_xml_object' => $offer_xml_object,
		'feed_imported_cat_arr' => $feed_imported_cat_arr,
		'site_product_id' => $site_product_id,
		'ipytw_product_was_sync' => ipytw_optionGET('ipytw_product_was_sync', $feedId, 'set_arr')
	);
	$res = ipytw_product_upd($args, $feedId);
 }
 if ($res === false) {
	return false;
 } else {
 	return $res;
 }
} // end function ipytw_unit($postId) {
?>